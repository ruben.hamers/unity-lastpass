# Unity-LastPass

An <b>Unofficial</b> LastPass Plugin for Unity3D.
It supports <b>Linux, Windows, Mac Standalone and Editor</b> plus <b>Android and iOS (Experimental)</b>.

##### <i>DISCLAIMER: USE AT OWN RISK!</i>
<b><i>I am far from a security expert but I tried my best to make it as secure as possible. However, there may be ways around my security measures I am unaware of!

Also; be aware that your account can be suspended temporarily when the incorrect credentials are entered (e.g. with Unit-Tests)!</i></b>

# Introduction
This is a [LastPass](https://lastpass.com) plugin for [Unity3D](https://Unity.com). With this library you can easily use LastPass for logging in to your accounts.
It is integrated in the Unity3D editor and it is usable at run-time on Windows, Linux and MacOS. It is also available for Android and iOS where iOS is experimental since I cannot build or test it.

The library offers the user the choice to use either a custom, [unofficial C# LastPass API](https://github.com/detunized/lastpass-sharp) for logging in (on all supported platforms), or the official [LastPass CLI](https://github.com/lastpass/lastpass-cli) (only for standalone/editor platforms).
My suggestion is to always use the CLI since it is probably more secure, however, on mobile platforms Unity-LastPass uses the custom library automatically. 
To use the CLI, some installation is required, which is all automated through EditorWindows in Unity. Please refer to the `Install` chapter for more information.

This README is structured in the following manner; First let's talk about how to start and setup your Unity-LastPass Config file. Second; installation of Unity-LastPass in the Unity Editor. Installing during runtime might be possible but is not implemented yet.
Next I will discuss the security measures I implemented to keep your information as safe as possible. Following this I will describe how to use the asset both editor-time and run-time. And Lastly, there are some small sections on testing, and Continuous Integration (CI). 

# Feedback - Known Issues - Feature Requests
For feedback, all known issues and feature requests please create a ticket or check the [GitLab ticket board](https://gitlab.com/ruben.hamers/unity-lastpass/-/boards).
Also, when you find bugs, please create tickets on the board. (One thing to keep in mind here is that when you __do not__ use the CLI integration, the lastpass login sequence is multi-threaded. I have tested it thoroughly on all platforms but there might still be race-conditions.
So, If logging in takes forever, and you do not see any exceptions it has most likely crashed in some thread in the thread-pool where exceptions are not thrown to the main thread)

# Setup
Unity-LastPass requires you to create a config file to keep some of the important settings used both at editor-time and run-time.
The Config file is a Scriptable Object (SO) and can be created through a menu shortcut: _Assets -> Create -> HamerSoft -> LastPass -> CreateConfig_
![Image of CreateConfig Path](Docs~/ConfigCreatePath.jpg)
This will create the SO and save it to your `Resources` folder. The SO **`MUST`** remain in a Resources folder for accessibility at runtime!

When you select the config it's inspector looks something like the following:
![Image of CreateConfig Path](Docs~/ConfigInspector.jpg)

There are several properties you are able to change to your liking; 

|Property|Description|
| :------------- | -----------: |
|TrustThisDevice|This flag will be used if you want to flag Unity-LastPass as a trusted source after logging in to your LastPass Account with Second Factor Authentication. This means LastPass will no longer prompt you for a one-time-passcode (OTP)|
|UseCli|Set to true if you want Unity-LastPass to use the LastPass-CLI instead of the Custom C# library. Note that for mobile platforms this will automatically be flagged to false (at run-time).|
|SecondFactorPassword (Prefab)|Prefab to spawn when Unity-LastPass must prompt the user for an OTP at run-time.|
|RunTimeLogin (Prefab)|Prefab to spawn when Unity-LastPass must prompt the user to login with his LastPass Master Credentials at run-time.|
|LoginOption|Different run-time Login options, `Automatic`: automatically logs in at run-time, `Trigger`: waits for the user to hit a certain key-bind(left-ctrl/left-cmd + left-shift + L) and `None` when you do not want any of the predefined login options. This way you will have to implement a custom login procedure.|
|Filters|Different kinds of filters that are run against your accounts (password data) like urls, groups, usernames.|
|Enable/Disable Android Support|Enable or disable Android support. Changing this setting will force Unity to recompile and load Android specific plugins & define symbols|
|Enable/Disable Android Support|Enable or disable iOS support. Changing this setting will force Unity to recompile and load iOS specific plugins & define symbols|

Upon construction of the Config SO these properties will be generated for you. The default prefabs are loaded from Resources folders internal to the Unity-LastPass asset. However, you can use custom made prefabs for this if you like.
The config will also try to generate some filters based on your project's CompanyName, ProductName, CloudService login email (says anonymous when not set).
You can add filters to this like: your target backend url which will then end up as a suggested password.

_*When, a login succeeds, the email that was used will automatically be added to the config. The filters of the config are then persisted (as json) on the Application.PersistentDataPath for later use when logging in again._  

# Installing
Installing is required for support of different command line interfaces (CLI). The installation process also differs for each host Operating System (OS). To quickly get a short overview, see the table below:

|Library|OS|Description|
| :------------- | :----------: | -----------: |
|[HomeBrew](https://brew.sh/)|MacOs|For installing Lastpass CLI|
|Python Keyring|Linux|For safely managing the lastpass master credentials.| 
|LastPass CLI|Windows, Linux, MacOS|For retrieving the list of accounts that match the given filters.|

I have tried my best to give the user (you) the best possible experience for the installation process, so when you open the LastPass Configuration in the menu bar at _HamerSoft -> LastPass -> Configuration_ you are guided through the installation process.

![Configuration Path](Docs~/LastPassConfiguration.jpg)

_*Some parts of the install process require your sudo password as input. If you are not happy with exposing your sudo password I added all commands that are executed in this README so you can run them yourself directly in the terminal._

_**Also note that this project is open-source so you can inspect all CLI code in the [CLI directory](src/Core/CLI)._

The following sections describe how the install process works:

## Windows
As the table suggests; Windows does not make use of a Keyring CLI for managing the LastPass Master Credentials. This is because C# allows access to the [Windows Credential Manager](https://www.techradar.com/news/what-is-windows-credential-manager) through a [simple API](https://www.nuget.org/packages/CredentialManagement). Credentials are stored safely within the Windows Credential Manager so there is no use in implementing a custom API or CLI.
On the other hand the LastPass-Cli is a totally different story. This CLI was not build with windows in mind and only runs on windows within [cygwin](https://www.cygwin.com/) linux, however fortunately, as of Windows 10 we can use the Ubuntu Sub-System!

The Windows installation is divided in multiple steps

### 1 - Installing the Ubuntu Sub system
I hope to automate this process but as for now please do the following:
If you have already installed and setup your Ubuntu Sub System the installation will be fully automatic, if not do the following:

Open the Windows Store app and Search for [Ubuntu](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab), Download and install it.

Next you need to activate it by opening a `PowerShell` Command Window `As ADMINISTRATOR` and running the following command and close the powershell window:

```Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux```

Then you can open Ubuntu and it will prompt you to create a user. 
When this process is done you should update all libraries (note that it might prompt your user's password):

```sudo apt-get update```

### 2 - Installing the LastPass-CLI
When you now open the Configuration it will verify that the Ubuntu sub-system is installed.
If it is properly installed the lastpass-cli will be installed fully automatically after you have entered your sudo password and pressed the install button.

If the process is not run automatically you run the following command in the Ubuntu terminal

```sudo -S apt-get --assume-yes install lastpass-cli```

The command above should work for `Ubuntu 20.04 LTS`, if not you can also enter the following command on the [lastpass-cli github page](https://github.com/lastpass/lastpass-cli). 
```
apt-get --no-install-recommends -yqq install \
  bash-completion \
  build-essential \
  cmake \
  libcurl4  \
  libcurl4-openssl-dev  \
  libssl-dev  \
  libxml2 \
  libxml2-dev  \
  libssl1.1 \
  pkg-config \
  ca-certificates \
  xclip
```
Now that you have installed the Ubuntu Sub-System and the LastPass-Cli you are ready to use Unity-LastPass.

## Linux
Linux requires a slightly different install process. We can simply use the linux terminal and run bash commands (so we do not need to install the Ubuntu Sub-System of course).
For linux we need to install two CLI, python keyring and the lastpass-cli. Now, you are probably a linux user so you might already have keyring installed, if so, you can go to step 2.
_Again, this process should be fully automated, but if some parts do not install correctly you can run the commands manually._

### 1 - Install python3-keyring
When you open the LastPassConfiguration window you will see button saying `Install Python Keyring`, with a field where you can enter your sudo password.
Upon pressing the button the following command is executed:
```
sudo -S apt-get --assume-yes install python3-keyring
```
### 2 - Install LastPass-CLI
This part of the installation on Linux executes the same command as during the Windows install.

## MacOS

The install process for MacOs differs slightly from linux and windows since MacOs already comes with a pre-installed keychain (security API).
All we need is to install the lastpass-cli and according to the docs, the easiest way is to install it with homebrew.
So the first step is to check if homebrew is installed, and if not, install it.

### 1 - Install HomeBrew
The Configuration window will show an install HomeBrew button when homebrew is not installed. Please enter your sudo password and press install to start installing.
_**Please note that installing HomeBrew takes quite a while so you might want to go grab a cup of coffee. Also, Currently the CLI processes do not run on another thread so they will freeze Unity.**_

If you want to enter the install command manually you can copy paste the following code in your terminal:

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

### 2 - Install lastpass-cli
Next we need to install the lastpass-cli with the following command:

```
brew install --build-from-source lastpass-cli
```
To be certain that HomeBrew is forced to recompile we use the `--build-from-source` prefix since there are many reports on the forums that there are issues with the lastpass-cli on osx.
If you get `Segmentation fault` errors you know that the install is not properly done.
During testing I could not get it to run properly on MacOs High Sierra, but on Catalina it works great.

# Mobile
To safely persist the LastPass Master Credentials on a mobile device I needed to use native libraries to access the host OS.
For Android I wrote a custom KeyChain like Unity library that can be found [here](https://gitlab.com/ruben.hamers/android-keychain) with the Android sources [here](https://gitlab.com/ruben.hamers/native-android-passwordmanager).

## Android
For android there are two things to keep note of:
* Enable Android support in Config; If you disable it, Unity will not compile the plugins!
* Set minimum API-Level to **22**
* In Android PlayerSettings, under `Other Settings` make sure **Internet access** is flagged to **required** just to make sure internet is available.


## IOS (EXPERIMENTAL)
For IOS I used a third-party plugin called [unity-ios-keychain-plugin](https://github.com/phamtanlong/unity-ios-keychain-plugin). This is a simple native IOS plugin to access the KeyChain.
* Enable iOS support in Config; If you disable it, Unity will not compile the plugins!

__**Please be aware that IOS is __experimental__ at this point in time since I do not own a MAC nor an IPhone.**__

# Security
Yeah, this is a tricky topic and I am very aware that this is difficult to get right, and if your LastPass Master credentials get exposed it can have major consequences.
That is why I tried to make everything as secure as I could, let me first summarize my measures and then dive deeper into each of them.

1. LastPass Master Credentials are stored in secure places offered by the OS or best practices.
2. When retrieving password suggestions Unity-LastPass will directly log out from LastPass when done.
3. Each CLI command runs in it's own process and is destroyed when the command completes.
4. Master Credentials are never kept in memory.
5. Only passwords that match the filters defined in the config are being used for suggestions.
6. Support for LastPass' Second Factor Authentication.

**1** So for each OS I implemented a system that is best practice or native to the host OS for storing sensitive information. For Windows this is the Credential Manager, for Linux and OSX this is the keychain. These utilities are made for securing sensitive information so I looked for a way to implement them.
I could not find any C# (Mono) libraries that gave access to the keychain on linux or OSX so I had to implement them through these CLI.

**2** When you login to your LastPas account in Unity-LastPass, the filters that are defined in the config are applied on all your accounts (passwords). The result of this filtering is a subset of accounts that match the filters and only those that actually match are kept as suggestions.
This means Unity-LastPass wil only keep credentials that match the filters in memory. (This also means that if you update filters or accounts you need to login again or UpdateAccounts through Unity-LastPass API)

**3** Each CLI command starts it's own process and closes it after the command is run, whether successful or not (I read both STDOut and STDError). That is why you will sometimes see multiple consoles flashing during the login sequence. I did not want to keep processes open while sudo passwords and LastPass credentials are used.

**4** Your LastPass Master Credentials are never kept in memory and are disposed immediately after being used (GC will collect them).

**5** As described before, upon logging in all your accounts are filtered based on the filters defined in the config. Only the ones that match are kept in memory as suggestions. This way passwords that have nothing to do with the actual app you are using are never kept in memory.

**6** If all the above measures might fail to secure the login process and passwords I added support for LastPass' second factor authentication. This way when you login you need a one-time-passcode for logging in ( You can enable trust this device of course).
 
# Usage
 This section will describe and show you how to use Unity-LastPass. The login process and several important prefabs are all standardized so you should be able to use Unity-LastPass right out of the box.
 You can of course create custom prefabs and even create a custom login process if you want to; this section will provide you with the knowledge how to do so.
 Let's first talk about the standardized prefabs and processes and then describe how to create your own custom process.

LastPass-Unity can be used straight in the Unity3D editor for a seamless login procedure. It can also be used in a compiled build, however I have not yet figured out how to do the installation at run-time.
(Meaning for example: On windows you need the Ubuntu sub system and lastpass-cli installed, I need to automate this at runtime.)

## Editor-Mode
Usage of Unity-LastPass inside the Unity3D editor is fairly straightforward; You can login through the _HamerSoft -> LastPass -> Configuration_ editor window.

![Login](Docs~/ConfigLogin.jpg)

When you login here, depending on whether you have second factor authentication enabled you will get a small popup window (in the center of you screen) where you can enter the OTP. 

![OTP](Docs~/OTP.jpg)

When you now start the editor and run your game your login will be fully automatic (depending on the Login Option in the config and if you trusted this device for second factor authentication).
_Unity-LastPass is build with a simple static class so it survives pressing Play & Stop in the editor, this also makes it easy to use when writing additional editor plugins. So if you have a plugin that needs to use Unity-LastPass you can access it easily without worrying about state._

## Run-Time
The usage of Unity-LastPass at run-time can be fully automatic. This depends on the selected login option in the config and if second factor authentication is trusted.
If, for some reason you have never logged in through the editor window described in the previous section the process goes like this:

**1** Depending on the login option, Unity-LastPass will try to log you in with your LastPass master credentials, if there are no credentials the RunTimeLogin screen is shown.
The default LoginRunTime prefab looks like the image below:

![RunTimeLogin](Docs~/RunTimeLogin.jpg)

Here you can login with you LastPass Master credentials. If you have second factor authentication enabled the SecondFactorPassword will be shown:

![SecondFactorPassword](Docs~/RunTimeOtpUi.jpg)

**2** Now you are logged in and you will be able to use Unity-LastPass at runtime.
When you select an input field that is marked as a Username or Password Field in the Form, the SuggestionUi will be shown:

![SuggestionUi](Docs~/SuggestionUi.jpg)

When you select a row in the UI the credentials will be used in the username and password fields.

### Custom prefabs
All run-time UI is fully customizable by you; as long as you make sure the required classes are attached to your prefabs they will work. In the previous section some prefab names were mentioned so let's sum them up:
 
|Prefab|Description|
| :-------------  | -----------: |
|RunTimeLogin|For logging in you LastPass Master Credentials|
|SecondFactorPassword|Run-time UI that prompts you for a one-time-passcode during the login process|
|Form|Class to attach to your login window to make it usable with Unity-LastPass|
|SuggestionUi|UI that is shown when a Username or Password marked field is selected to suggest credentials.|

_To change any of the prefabs, I suggest to simply duplicate them and then change the looks to your liking._

#### RunTimeLogin
The [RunTimeLogin](src/Ui/RuntimeLogin.cs) class is simple UI component (monobehaviour). You need to assign its inspector-public (serialized) fields and make sure it has UserName.cs and Password.cs classes attached to it's respective input-fields.
This window will popup when you need to login with the LastPass Master credentials. Also you need to assign this prefab in your Config.Asset file (which is located somewhere in your Resources folder(s)).

#### SecondFactorPassword
The [SecondFactorPassword](src/Core/OneTimePasscode/SecondFactorPassword.cs) is monobehaviour instantiated by the RuntimeOtpUi class. 
This Prefab will be spawned when a OTP is required. You can change the UI of this prefab as well, just make sure you assign it in your Config.Asset file.
 
#### Form
The [Form](src/Ui/Form.cs) is the default login form for Unity-LastPass. Through this UI passwords are suggested to UserName and Password fields.
Again, you can change the looks of this prefab and drop it somewhere in your scene, also make sure that the SuggestionUI prefab is assigned in it's inspector.

#### SuggestionUi
The [SuggestionUI](src/Ui/SuggestionUi.cs) is the popup that shows the passwords matching the filters defined in your config. This wil be used to actually fill in the usernames and passwords.
_* When you choose to change the looks of this prefab, make sure not to forget to change the [SuggestionElement](src/Ui/SuggestionElement.cs) as well since those are spawned as rows._

### Custom Implementation
Everything described above is all part of the default ui, and login flow. I am aware that many apps have a specific flow when it comes to starting/setup up, scene setup and, authentication/authorization.
When you want to implement a custom login you can use the API offered by [Unity-LastPass' (static) facade class](src/Core/LastPass.cs).
All public functions have been augmented with short method summaries to provide information on what they do (if these are unclear/confusing to you or you have feedback please make a ticket for me so I can change it).

### Example
An example of the entire login flow can be found in the [Example Scene](src/Example/LastPass.unity).
   
# (Unit) Testing
(Unit)-Testing in this project is somewhat interesting since it heavily depends on the target platform and sensitive information.
That is why I choose to implement unit testing that depends on a set of static variables what I call the [TestingEnv](src/Tests/Editor/TestingEnv.cs)ironment.
What this class does, is load a Json file called `TestingConfig.json` from your projects <b>Assets Root Directory</b> (Assets/TestingConfig.json).
This will be used for LastPass credentials, OS(Sudo) passwords. The TestingConfig.json looks something like this:
```json
{
  "LpUsername": "FooMasterEmail@gmail.com",
  "LpPassword": "FooMasterPassword",
  "LpOtpUserName": "BarMasterOTPEmail@gmail.com",
  "LpOtpPassword": "BarMasterOTPPassword",
  "OsPassword": "linux-sudo-password",
  "MacOsPassword": "mac-sudo-password",
  "WinBuntuOsPassword": "windows-ubuntu-sudo-password",
  "KeyringSystem": "Unity-LastPass-Testing",
  "ResourcesTestsPath": "Resources/Tests",
  "ConfigAssetName": "Config.asset"
}
```
_*The Continuous integration (CI) pipeline in gitlab has this file in its [CI-Variables](https://docs.gitlab.com/ee/ci/variables/) and will create this Json file in the Assets folder when the pipeline is started._
_**Please remember that running too many failing LastPass login requests will potentially, temporarily, suspend your account!_
  
# Continuous Integration
For now there is no CI configured since I'm unable to properly automate everything. Meaning the `automated` unit tests still require input from the tester, some integration tests like logging in with OTP are inherently dependent on user input.
However, there are Unit-tests that can be run in the unity editor, depending on your platform.