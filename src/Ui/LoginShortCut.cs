using UnityEngine;

namespace HamerSoft.LastPassUnity.Ui
{
    public class LoginShortCut : MonoBehaviour
    {
        private void Update()
        {
            if (!Core.LastPass.IsSupported || Core.LastPass.IsLoggedIn)
            {
                Destroy(gameObject);
                return;
            }
            
            #if UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX || UNITY_EDITOR_WIN
            if(Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.L))
                HamerSoft.LastPassUnity.Core.LastPass.Login();
            #elif UNITY_STANDALONE_OSX || UNITY_EDITOR
            if(Input.GetKey(KeyCode.LeftCommand) && Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.L))
                HamerSoft.LastPassUnity.Core.LastPass.Login();
            #endif
        }
    }
}