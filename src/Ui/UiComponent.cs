using System;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Ui
{
    public class UiComponent : MonoBehaviour
    {
        public event Action<UiComponent> Destroyed;
        
        protected virtual void Awake()
        {
        }

        protected virtual void Start()
        {
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnDisable()
        {
        }

        protected virtual void OnDestroy()
        {
            OnDestroyed();
        }

        private void OnDestroyed()
        {
            Destroyed?.Invoke(this);
        }
    }
}