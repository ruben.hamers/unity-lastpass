using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HamerSoft.LastPassUnity.Ui
{
    public class SuggestionUi : UiComponent
    {
        public event Action<Tuple<string, string>> Suggested, Initialized;

        [SerializeField] private SuggestionElement _suggestionPrefab;
        private Dropdown _unityDropdown;
        private VerticalLayoutGroup _group;
        private List<Tuple<string, string>> _suggestions;
        private Button _cancelButton;

        public void Show(List<Tuple<string, string>> suggestions)
        {
            _suggestions = suggestions;
            _group = GetComponentInChildren<VerticalLayoutGroup>();
            _cancelButton = GetComponentInChildren<Button>();
            _cancelButton.onClick.AddListener(() => Destroy(gameObject));
            CreateSuggestions();
            OnInitialized(suggestions.First());
        }

        private void CreateSuggestions()
        {
            if (!_suggestionPrefab)
                return;
            foreach (Tuple<string, string> suggestion in _suggestions)
            {
                var element = Instantiate(_suggestionPrefab, _group.transform);
                element.SetSuggestion(suggestion, OnSuggested);
            }
        }

        private void OnInitialized(Tuple<string, string> suggestion)
        {
            Initialized?.Invoke(suggestion);
        }

        private void OnSuggested(Tuple<string, string> suggestion)
        {
            Suggested?.Invoke(suggestion);
        }
    }
}