using System;
using HamerSoft.LastPassUnity.Core;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HamerSoft.LastPassUnity.Ui
{
    public class RuntimeLogin : UiComponent
    {
        [SerializeField] private Button _confirmButton, _cancelButton;
        [SerializeField] private Text _warning;
        private UserName _username;
        private Password _password;

        protected override void Awake()
        {
            base.Awake();
            _username = GetComponentInChildren<UserName>();
            _password = GetComponentInChildren<Password>();
            if (!AddOnClick(_confirmButton, Confirm))
                Debug.LogWarning("Confirm Button on the RuntimeLogin Form is not assigned!");
            if (!AddOnClick(_cancelButton, Cancel))
                Debug.LogWarning("Cancel Button on the RuntimeLogin Form is not assigned!");
        }

        private bool AddOnClick(Button button, UnityAction action)
        {
            if (!button)
                return false;
            button.onClick.AddListener(action);
            return true;
        }

        private void Cancel()
        {
            Destroy(gameObject);
        }

        private void Confirm()
        {
            TrySetWarning(string.Empty);
            if (!string.IsNullOrEmpty(_username.GetText()) && !string.IsNullOrEmpty(_password.GetText()))
                LastPassUnity.Core.LastPass.Login(new Credentials(_username.GetText(), _password.GetText()),
                    new RuntimeOtpUi(),
                    (success, exception) =>
                    {
                        if (success)
                            Destroy(gameObject);
                        else
                            TrySetWarning(exception);
                    });
            else
                TrySetWarning("Username or password is empty!");
        }

        private void TrySetWarning(Exception exception)
        {
            Debug.Log(exception);
            Debug.Log(exception.StackTrace);
            Debug.Log(exception.Message);
            TrySetWarning(exception.Message);
        }

        private void TrySetWarning(string text)
        {
            if (!_warning)
                return;
            _warning.text = text;
        }
    }
}