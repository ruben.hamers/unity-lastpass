using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace HamerSoft.LastPassUnity.Ui
{
    public class Form : UiComponent, ISelectHandler
    {
        [SerializeField, Tooltip("Parent transform to spawn the suggestion UI. When empty, will select first occurence of a canvas in the parent.")] private Transform _suggestionsParent;
        [SerializeField, Tooltip("Suggestion UI prefab")] private SuggestionUi _suggestionPrefab;
        private UserName _username;
        private Password _password;
        private SuggestionUi _suggestionUi;

        protected override void Awake()
        {
            base.Awake();
            if (!Core.LastPass.IsSupported)
            {
                Destroy(this);
                return;
            }

            RegisterFields();
        }

        public void OnSelect(BaseEventData eventData)
        {
            var suggestions = LastPassUnity.Core.LastPass.GetUserNamePasswordSuggestions();

            if (!_username || !_password)
                Debug.LogWarning("No UserName or PassWord Component(s) found!");
            else
                switch (suggestions.Count)
                {
                    case 0:
                        Debug.LogWarning("LastPass could not find any matching credentials!");
                        break;
                    case 1:
                        SetSuggestion(suggestions[0]);
                        break;
                    default:
                        if (!_suggestionUi)
                            ShowSuggestionUi(suggestions);
                        break;
                }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (_username)
                _username.Selected -= ChildSelected_Handler;
            if (_password)
                _password.Selected -= ChildSelected_Handler;
        }

        private void ShowSuggestionUi(List<Tuple<string, string>> suggestions)
        {
            var parent = GetParent();
            _suggestionUi = Instantiate(_suggestionPrefab, parent);
            _suggestionUi.transform.localPosition = _suggestionPrefab.transform.localPosition;
            _suggestionUi.Suggested += SuggestionUISuggested_Handler;
            _suggestionUi.Initialized += SuggestionUIInitialized_Handler;
            _suggestionUi.Destroyed += SuggestionUIDestroyed_Handler;
            _suggestionUi.Show(suggestions);
        }

        private Transform GetParent()
        {
            Transform parent = _suggestionsParent ? _suggestionsParent : GetComponentInParent<Canvas>()?.transform;
            if (parent)
                return parent;
            
            throw new NullReferenceException("No parent Transform or Canvas found to spawn the LastPass Suggestion UI!");
        }

        private void SetSuggestion(Tuple<string, string> suggestions)
        {
            _username.SetSuggestion(suggestions.Item1);
            _password.SetSuggestion(suggestions.Item2);
        }

        private void RegisterFields()
        {
            RegisterField(ref _username);
            RegisterField(ref _password);
        }

        private void RegisterField<T>(ref T field) where T : InputField
        {
            if (field)
                return;
            field = GetComponentInChildren<T>();
            field.Selected += ChildSelected_Handler;
            field.Destroyed += ChildDestroyed_Handler;
        }

        private void SuggestionUISuggested_Handler(Tuple<string, string> suggestion)
        {
            _suggestionUi.Suggested -= SuggestionUISuggested_Handler;
            _suggestionUi.Initialized -= SuggestionUIInitialized_Handler;
            _suggestionUi.Destroyed -= SuggestionUIDestroyed_Handler;
            Destroy(_suggestionUi.gameObject);
            SetSuggestion(suggestion);
        }

        private void SuggestionUIInitialized_Handler(Tuple<string, string> suggestion)
        {
            _suggestionUi.Initialized -= SuggestionUIInitialized_Handler;
            SetSuggestion(suggestion);
        }

        private void SuggestionUIDestroyed_Handler(UiComponent comp)
        {
            _suggestionUi.Suggested -= SuggestionUISuggested_Handler;
            _suggestionUi.Destroyed -= SuggestionUIDestroyed_Handler;
            _suggestionUi.Initialized -= SuggestionUIInitialized_Handler;
        }

        private void ChildSelected_Handler()
        {
            OnSelect(null);
        }

        private void ChildDestroyed_Handler(UiComponent comp)
        {
            (comp as InputField).Selected -= ChildSelected_Handler;
            comp.Destroyed -= ChildDestroyed_Handler;
        }
    }
}