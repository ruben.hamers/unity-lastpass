using System;
using TMPro;
using UnityEngine.EventSystems;

namespace HamerSoft.LastPassUnity.Ui
{
    public abstract class InputField : UiComponent, ISelectHandler
    {
        public event Action Selected;
        private UnityEngine.UI.InputField _unityInputField;
        private TMP_InputField _tmpInputField;

        protected override void Awake()
        {
            base.Awake();
            GetComponents();
            if (!Core.LastPass.IsSupported)
                Destroy(this);
        }

        public void SetSuggestion(string suggestion)
        {
            GetComponents();
            if (_unityInputField)
                _unityInputField.text = suggestion;
            if (_tmpInputField)
                _tmpInputField.text = suggestion;
        }

        private void GetComponents()
        {
            _unityInputField = GetComponent<UnityEngine.UI.InputField>();
            _tmpInputField = GetComponent<TMP_InputField>();
        }

        public void OnSelect(BaseEventData eventData)
        {
            OnSelected();
        }

        private void OnSelected()
        {
            Selected?.Invoke();
        }

        public string GetText()
        {
            if (_unityInputField)
                return _unityInputField.text;
            else if (_tmpInputField)
                return _tmpInputField.text;
            return string.Empty;
        }
    }
}