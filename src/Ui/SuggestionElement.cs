using System;
using UnityEngine;
using UnityEngine.UI;

namespace HamerSoft.LastPassUnity.Ui
{
    [RequireComponent(typeof(Button))]
    public class SuggestionElement : MonoBehaviour
    {
        private Button _button;
        private Action<Tuple<string, string>> _callback;
        private Tuple<string, string> _suggestion;

        protected void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(() => _callback?.Invoke(_suggestion));
        }

        public void SetSuggestion(Tuple<string, string> suggestion, Action<Tuple<string, string>> callback)
        {
            _suggestion = suggestion;
            _callback = callback;
        }
    }
    
}