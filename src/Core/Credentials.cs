using System;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core
{
    public class Credentials
    {
        public readonly string Username;
        public readonly string Password;
        public string Id { get; private set; }
        public string Description { get; private set; }
        public bool IsTrustedDevice { get; private set; }
        private const string SEPARATOR = "_Z_3_p_@_r_A_7_0_R_";

        public Credentials(string username, string password)
        {
            Username = username;
            Password = password;
            Id = "385e2742aefd399bd182c1ea4c1aac4b";
            Description = "LastPass-Unity Plugin";
            IsTrustedDevice = true;
        }

        public void SetId(string id)
        {
            if (Guid.TryParse(id, out var guid))
                Id = id;
        }

        public void SetDescription(string description)
        {
            if (!string.IsNullOrEmpty(description))
                Description = description;
        }

        public void SetIsTrustedDevice(bool isTrusted)
        {
            IsTrustedDevice = isTrusted;
        }

        public string Join()
        {
            return $"{Username}{SEPARATOR}{Password}";
        }

        public static Credentials Disjoin(string input)
        {
            try
            {
                string[] vals = input.Split(new string[] {SEPARATOR}, StringSplitOptions.RemoveEmptyEntries);
                return new Credentials(vals[0].Trim(), vals[1].Trim());
            }
            catch (Exception)
            {
                Debug.LogWarning(
                    "Could not parse username and password! Something went wrong logging in you master password to LastPass or credentials could not be found.");
                return null;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Credentials c &&
                   c.Username == Username &&
                   c.Password == Password;
        }

        public override int GetHashCode()
        {
            return string.Concat(Username, Password).GetHashCode();
        }
    }
}