using System;

namespace HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu
{
    public class WindowsUbuntuLastPassCli : AbstractLastPassCli
    {
        protected override string ProcessFileName => "ubuntu.exe";
    }
}