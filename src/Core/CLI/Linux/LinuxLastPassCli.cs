using System;
using HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu;

namespace HamerSoft.LastPassUnity.Core.CLI.Linux
{
    public class LinuxLastPassCli : AbstractLastPassCli, IDisposable
    {
        protected override string ProcessFileName => "/bin/bash";
    }
}