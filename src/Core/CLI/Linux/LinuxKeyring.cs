using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.CLI.Linux
{
    public class LinuxKeyring : AbstractLinuxCli
    {
        private string _keyringSystem = "Unity-LastPass";
        protected override string ProcessFileName => "/bin/bash";

        public LinuxKeyring(string keyringSystem)
        {
            _keyringSystem = keyringSystem;
        }

        public override bool IsInstalled(bool refresh = false)
        {
            if (IsInstallComplete != null && !refresh)
                return IsInstallComplete.Value;
            var result = RunCommand(string.Format(APT_INSTALLED_COMMAND, "python3-keyring"));
            IsInstallComplete = result.StartsWith("true");
            return IsInstallComplete.Value;
        }

        public override void Install(string osPassword, Action<bool> success)
        {
            bool canSudo = true;
            var result = string.Empty;
            if (HasRootAccess(out canSudo) || string.IsNullOrWhiteSpace(osPassword))
            {
                result = RunCommand(canSudo
                    ? "sudo -S apt-get --assume-yes install python3-keyring"
                    : "apt-get --assume-yes install python3-keyring");
            }
            else
            {
                result = RunCommand(canSudo
                    ? $"echo {osPassword} | sudo -S apt-get --assume-yes install python3-keyring"
                    : "apt-get --assume-yes install python3-keyring");
            }

            success?.Invoke(IsInstalled());
        }

        internal bool AddPassword(Credentials credentials)
        {
            try
            {
                string command = $"keyring set {_keyringSystem} {GetOsUserName()} <<STDIN\n{credentials.Join()}\nSTDIN\n";
                var result = RunCommand(command);
                return GetPassword() != null;
            }
            catch (CliException clie)
            {
                return false;
            }
        }

        internal Credentials GetPassword()
        {
            try
            {
                string result =
                    RunCommand(
                        $"keyring get {_keyringSystem} {GetOsUserName()}");
                List<string> lines = new List<string>();
                Match match = Regex.Match(result, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);
                while (match.Success)
                {
                    string line = match.Value;
                    if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line))
                        lines.Insert(0, line);
                    match = match.NextMatch();
                }

                return Credentials.Disjoin(lines.LastOrDefault());
            }
            catch (CliException)
            {
                return null;
            }
        }

        internal bool DeletePassword(Credentials credentials)
        {
            try
            {
                if (credentials == null)
                    return true;
                var output = RunCommand(new Queue<string>(new[]
                {
                    $"keyring del {_keyringSystem} {GetOsUserName()}",
                    credentials.Join()
                }));
                return GetPassword() == null;
            }
            catch (CliException clie)
            {
                return GetPassword() == null;
            }
        }
    }
}