using System;
using System.Collections.Generic;

namespace HamerSoft.LastPassUnity.Core.CLI
{
    public class CliException : Exception
    {
        public string StandardOut { get; private set; }
        public string StandardError { get; private set; }

        public CliException(string standardOut, string standardError)
        {
            StandardOut = standardOut;
            StandardError = standardError;
        }
    }

    public interface ICLI
    {
        bool HasRootAccess(out bool sudoInstalled);
        bool IsInstalled(bool refresh = false);
        void Install(string osPassword, Action<bool> success);
        string RunCommand(string command, bool waitForIdle = true);
        string RunCommand(Queue<string> commands, bool waitForIdle = true);
    }
}