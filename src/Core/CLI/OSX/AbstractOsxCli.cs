﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace HamerSoft.LastPassUnity.Core.CLI.Osx
{
    public abstract class AbstractOsxCli : ICLI
    {
        protected const string GREP_COMMAND = "{0} | grep -q \"^{1}\" && echo \"true\" || echo \"false\"";
        public abstract bool IsInstalled(bool refresh = false);
        public abstract void Install(string osPassword, Action<bool> success);
        protected const string ProcessFileName = "/bin/bash";

        protected bool? IsInstallComplete;

        public string RunCommand(Queue<string> commands, bool waitForIdle = true)
        {
            Process proc = new Process
            {
                StartInfo =
                {
                    FileName = ProcessFileName,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    RedirectStandardError = true
                }
            };

            StringBuilder output = new StringBuilder();
            try
            {
                proc.Start();
            }
            catch (Exception e)
            {
                throw new CliException(string.Empty, e.Message);
            }

            var writer = proc.StandardInput;
            if (waitForIdle)
                proc.WaitForInputIdle();
            while (commands.Count > 0)
            {
                writer.WriteLine(commands.Dequeue());
                if (waitForIdle)
                    proc.WaitForInputIdle();
                proc.Refresh();
            }

            writer.Close();
            string error = proc.StandardError.ReadToEnd();
            while (!proc.HasExited)
            {
            }

            output.Append(proc.StandardOutput.ReadToEnd());
            var exitCode = proc.ExitCode;
            proc.Close();

            if (exitCode >= 1 && !string.IsNullOrWhiteSpace(error) && string.IsNullOrWhiteSpace(output.ToString()))
                throw new CliException(output.ToString(), error);

            return output.ToString();
        }

        public string RunCommand(string command, bool waitForIdle = true)
        {
            Process proc = new Process
            {
                StartInfo =
                {
                    FileName = ProcessFileName,
                    Arguments = "-c \" " + command + " \"",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                }
            };

            StringBuilder output = new StringBuilder();
            try
            {
                proc.Start();
            }
            catch (Exception e)
            {
                throw new CliException(string.Empty, e.Message);
            }

            while (!proc.StandardOutput.EndOfStream)
                output.AppendLine(proc.StandardOutput.ReadLine());
            string error = proc.StandardError.ReadToEnd();
            while (!proc.HasExited)
            {
            }

            var exitCode = proc.ExitCode;
            proc.Close();
            if (exitCode >= 1 && !string.IsNullOrWhiteSpace(error) && string.IsNullOrWhiteSpace(output.ToString()))
                throw new CliException(output.ToString(), error);

            return output.ToString();
        }

        public bool HasRootAccess(out bool sudoInstalled)
        {
            try
            {
                var result = RunCommand($"grep {GetOsUserName()} /etc/sudoers");
                sudoInstalled = true;
                return !result.ToLower().Contains("denied");
            }
            catch (CliException clie)
            {
                sudoInstalled = !clie.StandardError.Contains("No such file or directory");
                return !sudoInstalled;
            }
        }

        protected virtual string GetOsUserName()
        {
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
return "";
#else
            return Environment.UserName;//return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
#endif
        }
    }
}