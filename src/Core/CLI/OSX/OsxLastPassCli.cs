using System;
using HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu;

namespace HamerSoft.LastPassUnity.Core.CLI.Osx
{
    public class OsxLastPassCli : AbstractLastPassCli
    {
        protected override string ProcessFileName => "/bin/bash";

        public override bool IsInstalled(bool refresh = false)
        {
            if (IsInstallComplete != null && !refresh)
                return IsInstallComplete.Value;
            var result = RunCommand("brew list | grep -q \"^lastpass-cli\" && echo \"true\" || echo \"false\"");
            IsInstallComplete = result.StartsWith("true");
            return IsInstallComplete.Value;
        }

        public override void Install(string osPassword, Action<bool> success)
        {
            var result = RunCommand($"brew install --build-from-source lastpass-cli", false);
            success?.Invoke(IsInstalled(true));
        }
    }
}