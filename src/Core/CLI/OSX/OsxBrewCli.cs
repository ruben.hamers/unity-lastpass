﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace HamerSoft.LastPassUnity.Core.CLI.Osx
{
    public class OsxBrewCli : AbstractOsxCli
    {
        public override bool IsInstalled(bool refresh = false)
        {
            if (IsInstallComplete != null && !refresh)
                return IsInstallComplete.Value;
            try
            {
                var result = RunCommand("brew -h");
                IsInstallComplete = !result.Contains("command not found");
            }
            catch (CliException clie)
            {
                IsInstallComplete = !clie.StandardError.Contains("command not found");
            }

            return IsInstallComplete.Value;
        }

        public override void Install(string osPassword, Action<bool> success)
        {
            try
            {
                string xcode = "xcode-select --install";
                string xcodeResult = RunCommand(xcode);
            }
            catch (CliException clie)
            {
                if (string.Format(GREP_COMMAND, clie.StandardError, "already installed").StartsWith("false"))
                {
                    Debug.Log($"Failed to install xcode-select --install with STDOUT {clie.StandardOut} and STDERROR {clie.StandardError}");
                    return;
                }
            }

            try
            {
                TryAddPassword(osPassword);
                string testCommand =
                    $"SUDO_ASKPASS=$(security find-generic-password -a {GetOsUserName()} -s {LastPass.KEYRING_SYSTEM}-hb -w) /bin/bash -c \"echo \"{osPassword}\" | sudo -S echo \"hi\"\r$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)\"";
                string result = RunCommand(new Queue<string>(new[]
                    {testCommand, "\r"}));
                IsInstalled(true);
            }
            catch (CliException clie)
            {
                Debug.Log($"Install homebrew Failed with STDOut {clie.StandardOut} and STDERROR {clie.StandardError}");
            }
        }

        private void TryAddPassword(string osPassword)
        {
            try
            {
                string addPassword = $"echo {osPassword} | sudo -S security add-generic-password -a {GetOsUserName()} -s {LastPass.KEYRING_SYSTEM}-hb -w {osPassword}";
                RunCommand(addPassword);
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}