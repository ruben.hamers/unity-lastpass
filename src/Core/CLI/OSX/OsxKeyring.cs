using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.CLI.Osx
{
    public class OsxKeyring : AbstractOsxCli
    {
        private string _keyringSystem = "Unity-LastPass";

        public OsxKeyring(string keyringSystem)
        {
            _keyringSystem = keyringSystem;
        }

        public override bool IsInstalled(bool refresh = false)
        {
            return true;
        }

        public override void Install(string osPassword, Action<bool> success)
        {
            success?.Invoke(IsInstalled());
        }

        internal bool AddPassword(Credentials credentials)
        {
            try
            {
                string command = $"security add-generic-password -a {GetOsUserName()} -s {LastPass.KEYRING_SYSTEM} -w {credentials.Join()}";
                RunCommand(command);
                return GetPassword() != null;
            }
            catch (CliException clie)
            {
                Debug.Log($"add password failed with: STDOUT {clie.StandardOut} and Error {clie.StandardError}");
                return GetPassword() != null;
            }
        }

        internal Credentials GetPassword()
        {
            try
            {
                string result =
                    RunCommand(
                        $"security find-generic-password -a {GetOsUserName()} -s {LastPass.KEYRING_SYSTEM} -w");
                List<string> lines = new List<string>();
                Match match = Regex.Match(result, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);
                while (match.Success)
                {
                    string line = match.Value;
                    if (!string.IsNullOrEmpty(line) && !string.IsNullOrWhiteSpace(line))
                        lines.Insert(0, line);
                    match = match.NextMatch();
                }

                return Credentials.Disjoin(lines.LastOrDefault());
            }
            catch (CliException clie)
            {
                return null;
            }
        }

        internal bool DeletePassword(Credentials credentials)
        {
            if (credentials == null)
                return true;

            try
            {
                var output = RunCommand($"security delete-generic-password -a {GetOsUserName()} -s {LastPass.KEYRING_SYSTEM}");
                return GetPassword() == null;
            }
            catch (CliException clie)
            {
                Debug.LogWarning(clie);
                return GetPassword() == null;
            }
        }
    }
}