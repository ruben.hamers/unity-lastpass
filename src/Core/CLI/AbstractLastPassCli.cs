using System;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu
{
    public abstract class AbstractLastPassCli : AbstractLinuxCli, IDisposable
    {
        public override bool IsInstalled(bool refresh = false)
        {
            if (IsInstallComplete != null && !refresh)
                return IsInstallComplete.Value;
            var result = RunCommand(string.Format(APT_INSTALLED_COMMAND, "lastpass-cli"));
            IsInstallComplete = result.StartsWith("true");
            return IsInstallComplete.Value;
        }

        public override void Install(string osPassword, Action<bool> success)
        {
            string result = String.Empty;
            bool canSudo = true;
            if (HasRootAccess(out canSudo) || string.IsNullOrWhiteSpace(osPassword))
                result = RunCommand(
                    canSudo
                        ? "sudo apt-get --assume-yes install lastpass-cli"
                        : "apt-get --assume-yes install lastpass-cli", false);
            else
                result = RunCommand(
                    canSudo
                        ? $"echo {osPassword} | sudo -S apt-get --assume-yes install lastpass-cli"
                        : "apt-get --assume-yes install lastpass-cli", false);
            success?.Invoke(IsInstalled(true));
        }

        public void Dispose()
        {
        }

        internal void Login(Credentials credentials, OtpUi otpUi, Action<bool, Exception> callback)
        {
            if (callback == null)
                return;

            try
            {
                var result = RunCommand(
                    $"echo {credentials.Password} | LPASS_DISABLE_PINENTRY=1 lpass login {(credentials.IsTrustedDevice ? "--trust " : "")}--force {credentials.Username}"
                    , false);
                callback.Invoke(true, null);
            }
            catch (CliException e)
            {
                LoginOtp(e, credentials, otpUi, callback);
            }
        }

        private void LoginOtp(CliException e, Credentials credentials, OtpUi otpUi, Action<bool, Exception> callback)
        {
            if (e != null && (e.StandardError.Contains("Aborted multifactor authentication") ||
                              e.StandardError.Contains("Authenticator Code")))
                otpUi.ProvideSecondFactorPassword(global::LastPass.Ui.SecondFactorMethod.GoogleAuth, otp =>
                {
                    try
                    {
                        string command =
                            $"LPASS_DISABLE_PINENTRY=1 lpass login {(credentials.IsTrustedDevice ? "--trust " : "")}--force {credentials.Username} <<STDIN\n{credentials.Password}\n{otp}\nSTDIN\n";
                        var res = RunCommand(command, false);
                        callback.Invoke(true, null);
                    }
                    catch (CliException clie)
                    {
                        LoginOtp(clie, credentials, otpUi, callback);
                    }
                }, () => { callback.Invoke(false, new NullReferenceException("Otp Cancelled!")); });
            else
                callback.Invoke(false, e);
        }

        internal void Logout()
        {
            var result = RunCommand("lpass logout --force", false);
        }

        internal string Export()
        {
            return RunCommand("lpass export --sync=now", false);
        }
    }
}