using System;
using LastPass;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace HamerSoft.LastPassUnity.Core
{
    public class SecondFactorPassword : MonoBehaviour
    {
        [SerializeField] private Button _confirmButton, _cancelButton;
        public event Action<string> Closed;

        private Text _warning;
        private TMP_Text _warningTmp;
        private InputField _secondFactorPassword;
        private TMP_InputField _secondFactorPasswordTmp;
        private UnityAction _cancelCallback;

        /// <summary>
        /// otp method; unsure what to do with this for now.
        /// </summary>
        private global::LastPass.Ui.SecondFactorMethod _method;

        protected virtual void Awake()
        {
            _warning = GetComponentInChildren<Text>();
            _warningTmp = GetComponentInChildren<TMP_Text>();
            _secondFactorPassword = GetComponentInChildren<InputField>();
            _secondFactorPasswordTmp = GetComponentInChildren<TMP_InputField>();
            AssignInputFields();
            AssignButtons();
        }

        private void AssignButtons()
        {
            if (_confirmButton)
                _confirmButton.onClick.AddListener(Confirmed);
            if (_cancelButton && _cancelCallback != null)
                _cancelButton.onClick.AddListener(_cancelCallback);
        }

        private void UnAssignButtons()
        {
            if (_confirmButton)
                _confirmButton.onClick.RemoveListener(Confirmed);
            if (_cancelButton && _cancelCallback != null)
                _cancelButton.onClick.RemoveListener(_cancelCallback);
        }

        private void AssignInputFields()
        {
            if (_secondFactorPassword)
                _secondFactorPassword.onEndEdit.AddListener(SecondFactorPassWordEditEnd);
            else if (_secondFactorPasswordTmp)
                _secondFactorPasswordTmp.onEndEdit.AddListener(SecondFactorPassWordEditEnd);
        }

        private void UnAssignInputFields()
        {
            if (_secondFactorPassword)
                _secondFactorPassword.onEndEdit.RemoveListener(SecondFactorPassWordEditEnd);
            else if (_secondFactorPasswordTmp)
                _secondFactorPasswordTmp.onEndEdit.RemoveListener(SecondFactorPassWordEditEnd);
        }

        private void Confirmed()
        {
            UnAssignButtons();
            UnAssignInputFields();
            OnClosed(GetSecondFactorText());
        }

        private void SecondFactorPassWordEditEnd(string value)
        {
            UnAssignButtons();
            UnAssignInputFields();
            OnClosed(value);
        }

        private void OnClosed(string value)
        {
            Closed?.Invoke(value);
        }

        public void SetMethod(global::LastPass.Ui.SecondFactorMethod method)
        {
            _method = method;
        }

        public void ShowWarning(string message)
        {
            AssignInputFields();
            AssignButtons();
            if (_warning)
                _warning.text = message;
            if (_warningTmp)
                _warningTmp.text = message;
            SetSecondFactorText();
        }

        private void SetSecondFactorText()
        {
            if (_secondFactorPassword)
                _secondFactorPassword.text = String.Empty;
            else if (_secondFactorPasswordTmp)
                _secondFactorPasswordTmp.text = String.Empty;
        }

        private string GetSecondFactorText()
        {
            if (_secondFactorPassword)
                return _secondFactorPassword.text;
            else if (_secondFactorPasswordTmp)
                return _secondFactorPasswordTmp.text;
            return String.Empty;
        }

        public void SetCancelled(Action cancelledCallback)
        {
            _cancelCallback = () =>
            {
                cancelledCallback?.Invoke();
                Destroy(gameObject);
            };
            if (_cancelButton)
                _cancelButton.onClick.AddListener(_cancelCallback);
        }
    }
}