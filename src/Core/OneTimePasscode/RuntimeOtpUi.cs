using System;
using System.Linq;
using LastPass;
using UnityEngine;
using Object = UnityEngine.Object;
using HamerSoft.Threading;

namespace HamerSoft.LastPassUnity.Core
{
    public abstract class OtpUi : global::LastPass.Ui
    {
        public abstract void ShowWarning(string message);
    }

    public class RuntimeOtpUi : OtpUi
    {
        private SecondFactorPassword _secondFactorInstance;
        private const string ApproveOutOfBandPath = "Lastpass/ApproveOutOfBand";

        public override string ProvideSecondFactorPassword(SecondFactorMethod method)
        {
            string result = null;

            void Callback(string otp)
            {
                _secondFactorInstance.Closed -= Callback;
                result = otp;
            }

            if (_secondFactorInstance != null)
            {
                _secondFactorInstance.Closed -= Callback;
                Object.DestroyImmediate(_secondFactorInstance);
            }

            InstantiateSecondFactor(method, null);
            if (_secondFactorInstance)
            {
                _secondFactorInstance.Closed += Callback;
                while (string.IsNullOrEmpty(result))
                {
                }
            }
            else
                Callback("");

            return result;
        }

        public override void ProvideSecondFactorPasswordAsync(SecondFactorMethod method, Action<string> callback,
            Action cancelledCallback)
        {
            Dispatcher.ToMainThread(() =>
            {
                if (Application.isPlaying)
                    ShowRuntimeUiAsync(method, callback, cancelledCallback);
                else
                    throw new Exception(
                        "Cannot use OtpUI in editor, please use the EditorOtp class for entering OTP in editor mode!");
            });
        }

        public override void ProvideSecondFactorPassword(SecondFactorMethod method, Action<string> callback,
            Action cancelledCallback)
        {
            if (Application.isPlaying)
                ShowRuntimeUi(method, callback, cancelledCallback);
            else
                throw new Exception(
                    "Cannot use OtpUI in editor, please use the EditorOtp class for entering OTP in editor mode!");
        }

        private void ShowRuntimeUi(SecondFactorMethod method, Action<string> callback, Action cancelledCallback)
        {
            void Callback(string otp)
            {
                try
                {
                    callback?.Invoke(otp);
                    _secondFactorInstance.Closed -= Callback;
                    Object.DestroyImmediate(_secondFactorInstance.gameObject);
                }
                catch (Exception e)
                {
                    ParseException(e);
                }
            }

            if (_secondFactorInstance != null)
            {
                _secondFactorInstance.Closed -= Callback;
                Object.DestroyImmediate(_secondFactorInstance.gameObject);
            }

            InstantiateSecondFactor(method, cancelledCallback);
            if (_secondFactorInstance)
                _secondFactorInstance.Closed += Callback;
            else
                callback?.Invoke("");
        }

        private void ShowRuntimeUiAsync(SecondFactorMethod method, Action<string> callback, Action cancelledCallback)
        {
            void Callback(string otp)
            {
                Dispatcher.RunAsync(() => { callback?.Invoke(otp); })
                    .ContinueWith(task => Dispatcher.ToMainThread(() =>
                    {
                        if (task.Exception?.InnerException != null)
                            ParseException(task.Exception.InnerException);
                        else
                        {
                            _secondFactorInstance.Closed -= Callback;
                            Object.DestroyImmediate(_secondFactorInstance.gameObject);
                        }
                    }));
            }

            if (_secondFactorInstance != null)
            {
                _secondFactorInstance.Closed -= Callback;
                Object.DestroyImmediate(_secondFactorInstance.gameObject);
            }

            InstantiateSecondFactor(method, cancelledCallback);
            if (_secondFactorInstance)
                _secondFactorInstance.Closed += Callback;
            else
                Dispatcher.RunAsync(() => { callback?.Invoke(""); });
        }

        public override void AskToApproveOutOfBand(OutOfBandMethod method)
        {
            Debug.Log("Approve out of band!?");
        }

        private void ParseException(Exception e)
        {
            if (e is LoginException loginException)
            {
                if (loginException.Reason == LoginException.FailureReason.LastPassIncorrectGoogleAuthenticatorCode
                    ||
                    loginException.Reason == LoginException.FailureReason.LastPassIncorrectYubikeyPassword
                    ||
                    loginException.Reason == LoginException.FailureReason.LastPassOther)
                {
                    ShowWarning("Incorrect OTP!");
                    Debug.LogError("The provided one-time-passcode is incorrect! Please enter a correct OTP.");
                }

                Debug.LogWarning(loginException.Message);
            }
            else
            {
                Debug.LogError(e);
                ShowWarning(e.Message);
            }
        }

        public override void ShowWarning(string message)
        {
            try
            {
                _secondFactorInstance.ShowWarning(message);
            }
            catch (Exception e)
            {
                // ignored
            }
        }

        protected virtual void InstantiateSecondFactor(SecondFactorMethod method, Action cancelledCallback)
        {
            var canvas = GetCanvas();
            if (!canvas)
            {
                Debug.Log(
                    "Could not instantiate SecondFactorPassword UI, there is no Canvas in ScreenSpace-Overlay or ScreenSpace-Camera");
                return;
            }

            _secondFactorInstance = Object.Instantiate(Config.Load().SecondFactorPassword, canvas.transform);
            _secondFactorInstance.SetMethod(method);
            _secondFactorInstance.SetCancelled(cancelledCallback);
            _secondFactorInstance.transform.SetAsLastSibling();
        }

        /// <summary>
        /// Dirty but works until I find a cleaner solution. (I do not want to create some dependency to the Form or canvas through static references or singletons for example)
        /// </summary>
        private static Canvas GetCanvas()
        {
            return Object.FindObjectsOfType<Canvas>().FirstOrDefault(c => c.renderMode != RenderMode.WorldSpace);
        }
    }
}