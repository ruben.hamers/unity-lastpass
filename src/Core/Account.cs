namespace HamerSoft.LastPassUnity.Core
{
    internal class Account
    {
        internal string Id { get; }
        internal string Name { get; }
        internal string Username { get; }
        internal string Password { get; }
        internal string Url { get; }
        internal string Group { get; }

        internal Account(string id, string name, string username, string password, string url, string @group)
        {
            Id = id;
            Name = name;
            Username = username;
            Password = password;
            Url = url;
            Group = @group;
        }
    }
}