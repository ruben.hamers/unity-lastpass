using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.LastPassUnity.Core.CLI;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using HamerSoft.LastPassUnity.Core.CLI.Osx;
using HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu;
using HamerSoft.LastPassUnity.Ui;
using UnityEngine;
using Object = UnityEngine.Object;

#if LP_IOS
using IosKeyChain;
#endif

namespace HamerSoft.LastPassUnity.Core
{
    public static class LastPass
    {
        /// <summary>
        /// The keyring system used for Unity-LastPass
        /// </summary>
        public const string KEYRING_SYSTEM = "Unity-LastPass";

        /// <summary>
        /// Android ifdef key
        /// </summary>
        public const string ANDROID_PLATFORM = "LP_ANDROID";

        /// <summary>
        /// iOS ifdef key
        /// </summary>
        public const string IOS_PLATFORM = "LP_IOS";

        /// <summary>
        /// Check if Unity-LastPass is logged in
        /// </summary>
        public static bool IsLoggedIn => _vault != null;

        /// <summary>
        /// Are there any LastPass Master Credentials known on this device
        /// </summary>
        public static bool HasCredentials
        {
            get
            {
                using (var mpw = new MasterPasswordManager())
                    return mpw.Load() != null;
            }
        }

        private static Vault _vault;
        private static ICLI _keyringCli, _lastPassCli, _homeBrewCli;

        /// <summary>
        /// Is Unity-LastPass supported on the current RunTimePlatform
        /// </summary>
        public static bool IsSupported
        {
            get
            {
                switch (Application.platform)
                {
                    case RuntimePlatform.OSXEditor:
                    case RuntimePlatform.OSXPlayer:
                    case RuntimePlatform.WindowsPlayer:
                    case RuntimePlatform.WindowsEditor:
                    case RuntimePlatform.LinuxPlayer:
                    case RuntimePlatform.LinuxEditor:
#if LP_ANDROID
                    case RuntimePlatform.Android:
#endif
#if LP_IOS
                    case RuntimePlatform.IPhonePlayer:
#endif
                        return true;
                    default:
                        return false;
                }
            }
        }

        /// <summary>
        /// Is the Unity-LastPass plugin installed correctly and completely
        /// </summary>
        /// <param name="refresh">force a refresh of the check (when false, it will used a cached value from a previous check)</param>
        /// <returns>true if the install is complete on your platform</returns>
        public static bool IsInstallComplete(bool refresh = false)
        {
            PerformStandAloneOrEditorAction(
                () =>
                {
                    if (_keyringCli != null && _lastPassCli != null && _homeBrewCli != null)
                        return;
                    _keyringCli = new OsxKeyring(KEYRING_SYSTEM);
                    _lastPassCli = new OsxLastPassCli();
                    _homeBrewCli = new OsxBrewCli();
                },
                () =>
                {
                    if (_lastPassCli == null)
                        _lastPassCli = new WindowsUbuntuLastPassCli();
                },
                () =>
                {
                    if (_keyringCli != null && _lastPassCli != null)
                        return;
                    _keyringCli = new LinuxKeyring(KEYRING_SYSTEM);
                    _lastPassCli = new LinuxLastPassCli();
                });

            return PerformStandAloneOrEditorAction(
                () => _homeBrewCli?.IsInstalled(refresh) == true && _lastPassCli?.IsInstalled(refresh) == true,
                () => _lastPassCli?.IsInstalled(refresh) == true,
                () => _keyringCli?.IsInstalled(refresh) == true && _lastPassCli?.IsInstalled(refresh) == true);
        }

        /// <summary>
        /// Login to Unity-LastPass (The Config will be loaded from resources)
        /// </summary>
        /// <param name="credentials">The credentials to use</param>
        /// <param name="otpUi">OTP ui, either runtime or editor time</param>
        /// <param name="callback">callback to invoke when complete</param>
        public static void Login(Credentials credentials, OtpUi otpUi, Action<bool, Exception> callback)
        {
            if (IsLoggedIn)
            {
                callback.Invoke(true, null);
                return;
            }

            var newVault = new Vault();
            newVault.UpdateConfig(Config.Load());
            newVault.Login(credentials, otpUi, (success, exception) =>
            {
                if (success)
                {
                    _vault = newVault;
                    using (MasterPasswordManager mpm = new MasterPasswordManager(credentials))
                    {
                        var existingCredentials = mpm.Load();
                        if (existingCredentials == null || !existingCredentials.Equals(credentials))
                            if (!mpm.Save(credentials))
                                Debug.LogWarning("Failed to save master password to host system!");
                    }
                }

                callback?.Invoke(success, exception);
            });
        }

        /// <summary>
        /// Login to Unity-LastPass (The Credentials will be loaded from Keyring or Credential Manager)
        /// </summary>
        /// <param name="config">The config scriptable object</param>
        /// <param name="otpUi">OTP ui, either runtime or editor time</param>
        /// <param name="callback">callback to invoke when complete</param>
        public static void Login(Config config, OtpUi otpUi, Action<bool, Exception> callback)
        {
            if (IsLoggedIn)
            {
                callback.Invoke(true, null);
                return;
            }

            var newVault = new Vault();
            newVault.UpdateConfig(config);
            var credentials = new MasterPasswordManager(null).Load();
            credentials?.SetIsTrustedDevice(config.TrustThisDevice);
            if (credentials == null)
                if (Application.isPlaying)
                    ShowRuntimeLogin(config, callback);
                else
                    callback.Invoke(false,
                        new NotSupportedException(
                            "Please login through the supplied UnityEditor Menu Item. (HamerSoft/LastPass/Configuration)"));
            else
                newVault.Login(credentials, otpUi, (success, exception) =>
                {
                    if (success)
                        _vault = newVault;
                    else
                        using (var manager = new MasterPasswordManager(credentials))
                            manager.Delete();

                    credentials = null;
                    callback?.Invoke(success, exception);
                });
        }

        /// <summary>
        /// Login to Unity-LastPass (The Config will be loaded from resources, and Credentials from Keyring or Credential Manager)
        /// </summary>
        /// <param name="otpUi">OTP ui, either runtime or editor time</param>
        /// <param name="callback">callback to invoke when complete</param>
        public static void Login(OtpUi otpUi, Action<bool, Exception> callback)
        {
            if (IsLoggedIn)
            {
                callback.Invoke(true, null);
                return;
            }

            var config = Config.Load();
            if (!config)
            {
                callback.Invoke(false, new NullReferenceException("No Config found for LastPass Login!"));
                return;
            }

            Login(config, otpUi, callback);
        }

        /// <summary>
        /// Login to Unity-LastPass (RUN-TIME ONLY)
        /// This will show the RunTimeLogin ui prefab
        /// </summary>
        /// <param name="callback">callback to invoke when complete</param>
        public static void Login(Action<bool, Exception> callback)
        {
            if (IsLoggedIn)
            {
                callback.Invoke(true, null);
                return;
            }

            if (!Application.isPlaying)
            {
                callback.Invoke(false, new NotSupportedException("This login method is for Runtime only!"));
                return;
            }

            ShowRuntimeLogin(Config.Load(), callback);
        }

        private static void ShowRuntimeLogin(Config config, Action<bool, Exception> callback)
        {
            if (IsLoggedIn)
            {
                callback.Invoke(true, null);
                return;
            }

            if (!config.RuntimeLogin)
            {
                callback.Invoke(false, new NullReferenceException("RuntimeLogin prefab on the Config object is null!"));
                return;
            }

            var canvas = Object.FindObjectsOfType<Canvas>().FirstOrDefault(c => c.renderMode != RenderMode.WorldSpace);
            if (canvas)
            {
                Object.Instantiate(config.RuntimeLogin, canvas.transform);
                callback.Invoke(false, new NullReferenceException("No Credentials found on host system."));
            }
            else
            {
                callback.Invoke(false, new NullReferenceException("No canvas found to spawn the LastPass Login UI!"));
            }
        }

        internal static void Login()
        {
            if (IsLoggedIn)
                return;
            Config config = Config.Load();
            if (config)
                Login(config, new RuntimeOtpUi(), (success, exception) =>
                {
                    Debug.Log(success
                        ? "<color=red>[LastPass]:</color> <color=black>Successfully logged in with master password!</color>"
                        : $"<color=red>LastPass Login Unsuccessful with exception {exception} and message: {exception.Message}</color>");
                });
        }

        /// <summary>
        /// Force a logout from Unity-LastPass
        /// </summary>
        public static void Logout()
        {
            if (!IsLoggedIn)
                return;

            _vault.Dispose();
            _vault = null;
            using (MasterPasswordManager mpm = new MasterPasswordManager())
            {
                mpm.Load();
                mpm.Delete();
            }
        }

        /// <summary>
        /// Update accounts to suggest
        /// </summary>
        /// <param name="otpUi">the otp ui to use, either run-time or editor-time</param>
        /// <param name="callback">callback that is invoked when done</param>
        public static void UpdateAccounts(OtpUi otpUi, Action<bool, Exception> callback = null)
        {
            if (!IsLoggedIn)
                return;
            var credentials = new MasterPasswordManager(null).Load();
            if (credentials == null)
            {
                callback?.Invoke(false, new NullReferenceException("Cannot update accounts since there is no known master credentials for login!"));
                return;
            }

            var config = Config.Load();
            if (!config)
            {
                callback.Invoke(false, new NullReferenceException("No Config found for LastPass Login!"));
                return;
            }

            credentials.SetIsTrustedDevice(config.TrustThisDevice);
            _vault.UpdateAccounts(credentials, otpUi, (success, exception) =>
            {
                if (!success)
                    using (var manager = new MasterPasswordManager(credentials))
                        manager.Delete();

                callback?.Invoke(success, exception);
            });
        }

        /// <summary>
        /// Returns all suggestions in a tuple<string (username),string (password)> 
        /// </summary>
        /// <returns>all suggestions <username,password></returns>
        public static List<Tuple<string, string>> GetUserNamePasswordSuggestions()
        {
            return _vault == null ? new List<Tuple<string, string>>() : _vault.GetSuggestions();
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void UnitySceneStarted()
        {
            if (_vault != null)
                return;

#if LP_IOS
            KeyChain.Initialize();
#endif
            Config config = Config.Load();
            bool allowLogin = (IsSupported || Application.isEditor) && config.LoginOption == LoginOption.Automatic;
            if(!IsSupported)
                Debug.LogWarning($"Lastpass not supported or support disabled on {Application.platform}.");
            else if (allowLogin)
                Login(config, new RuntimeOtpUi(), (success, exception) =>
                {
                    Debug.Log(success
                        ? "<color=red>[LastPass]: </color><color=black>Successfully logged in with master password or prompting the user with login form!</color>"
                        : $"<color=red>[LastPass]: </color><color=black>Login Unsuccessful with exception {exception} and message: {exception.Message}</color>");
                });
            else if (IsSupported && config.LoginOption == LoginOption.Trigger)
                new GameObject("LastPassLogin_Shortcut").AddComponent<LoginShortCut>();
        }

        #region Utils

        public static void PerformStandAloneOrEditorAction(Action osxAction, Action winAction, Action linuxAction)
        {
            switch (Application.platform)
            {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer:
                    osxAction?.Invoke();
                    break;
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.WindowsEditor:
                    winAction?.Invoke();
                    break;
                case RuntimePlatform.LinuxPlayer:
                case RuntimePlatform.LinuxEditor:
                    linuxAction?.Invoke();
                    break;
            }
        }

        public static T PerformStandAloneOrEditorAction<T>(Func<T> osxAction, Func<T> winAction, Func<T> linuxAction)
        {
            switch (Application.platform)
            {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer:
                    return osxAction == null ? default : osxAction.Invoke();
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.WindowsEditor:
                    return winAction == null ? default : winAction.Invoke();
                case RuntimePlatform.LinuxPlayer:
                case RuntimePlatform.LinuxEditor:
                    return linuxAction == null ? default : linuxAction.Invoke();
            }

            return default;
        }

        #endregion
    }
}