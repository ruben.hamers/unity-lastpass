#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using System.Linq;
using System;
using HamerSoft.LastPassUnity.Ui;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core
{
    public enum LoginOption
    {
        Automatic,
        Off,
        Trigger
    }

    [CreateAssetMenu(fileName = "LastPassConfig", menuName = "HamerSoft/LastPass/CreateConfig", order = 0)]
    public class Config : ScriptableObject
    {
        [Tooltip("Trust this device for OTP.")]
        public bool TrustThisDevice = true;

        [Tooltip("Use the LastPass CLI instead of custom LastPass API.")]
        public bool UseCli = true;

        [Tooltip("The UI to show when LastPass Prompts the user for a second factor password (OTP).")]
        public SecondFactorPassword SecondFactorPassword;

        [Tooltip("The UI to show when LastPass Prompts the user to login with Master Credentials during runtime.")]
        public RuntimeLogin RuntimeLogin;

        [Tooltip("Allow use of LastPass at runtime, automatically or by trigger.")]
        public LoginOption LoginOption = LoginOption.Off;

        [Tooltip("Filters to use for suggesting credentials.")]
        public string[] Filters;

        [HideInInspector, SerializeField] public bool AndroidSupported, IosSupported;

        private const string SecondFactorPasswordPath = "LastPass/SecondFactorPassword";
        private const string RuntimeLoginPath = "LastPass/RuntimeLoginForm";

        public static Config Load(string resoucesPath = "LastPass/Config")
        {
            var config = Resources.Load<Config>(resoucesPath);
            var filters = LoadFilters();
            if (filters.Length > 0)
                config.Filters = filters;
            return config;
        }

        public void AddFilter(string filter)
        {
            if (Filters.Contains(filter))
                return;

            Filters = Filters.Concat(new[] {filter}).ToArray();
            PersistFilters(Filters);
        }

        private static string[] LoadFilters()
        {
            try
            {
                string filePath = Path.Combine(Application.persistentDataPath, "UnityLastPass", "Filters.json");
                if (!File.Exists(filePath))
                    return new string[0];

                using (StreamReader sr = new StreamReader(filePath))
                {
                    var filters = sr.ReadToEnd();
                    return JsonUtility.FromJson<string[]>(filters);
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning(
                    $"[LastPass]: Failed to persist new filters for UnityLastPass with exception {e} | {e.Message}");
            }

            return new string[0];
        }

        private static void PersistFilters(string[] filters)
        {
            try
            {
                var filterJson = JsonUtility.ToJson(filters);
                var directory = Path.Combine(Application.persistentDataPath, "UnityLastPass");
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);
                using (StreamWriter sw = new StreamWriter(Path.Combine(directory, "Filters.json")))
                    sw.Write(filterJson);
            }
            catch (Exception)
            {
                Debug.LogWarning("Failed to persist new filters for UnityLastPass");
            }
        }

#if UNITY_EDITOR
        public static Config Create(string resourcesPath)
        {
            if (Load() is Config c)
                return c;
            var fullPath = Path.Combine(Application.dataPath, resourcesPath);
            CreateDirectory(fullPath);
            return SaveConfig(CreateConfigInstance(), resourcesPath);
        }

        private static Config CreateConfigInstance()
        {
            var config = ScriptableObject.CreateInstance<Config>();
            config.TrustThisDevice = true;
            config.Filters = new[]
            {
                PlayerSettings.companyName, PlayerSettings.productName, CloudProjectSettings.organizationName,
                CloudProjectSettings.projectName, CloudProjectSettings.userName
            };
            config.Filters = config.Filters.Where(f => !string.IsNullOrEmpty(f)).ToArray();
            config.SecondFactorPassword = Resources.Load<SecondFactorPassword>(SecondFactorPasswordPath);
            config.RuntimeLogin = Resources.Load<RuntimeLogin>(RuntimeLoginPath);
            config.AndroidSupported = true;
            config.IosSupported = true;
            AdjustPlayerSettings();
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return config;
        }

        private static void AdjustPlayerSettings()
        {
            PlayerSettings.Android.forceInternetPermission = true;
           SetIfDev(BuildTargetGroup.Android, LastPassUnity.Core.LastPass.ANDROID_PLATFORM);
           SetIfDev(BuildTargetGroup.iOS, LastPassUnity.Core.LastPass.IOS_PLATFORM);
        }

        private static void SetIfDev(BuildTargetGroup platform, string ifdef)
        {
            var currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(platform);
            if (!currentDefines.Contains(ifdef))
                if (string.IsNullOrEmpty(currentDefines))
                    PlayerSettings.SetScriptingDefineSymbolsForGroup(platform, $"{ifdef};");
                else
                    PlayerSettings.SetScriptingDefineSymbolsForGroup(platform, $"{currentDefines};{ifdef};");
        }

        private static Config SaveConfig(Config config, string resourcesPath)
        {
            string filename = Path.Combine("Assets", resourcesPath, "Config.asset");
            AssetDatabase.CreateAsset(config, filename);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return AssetDatabase.LoadAssetAtPath<Config>(filename);
        }

        private static void CreateDirectory(string fullPath)
        {
            if (!Directory.Exists(fullPath))
                Directory.CreateDirectory(fullPath);
        }
#endif
    }
}