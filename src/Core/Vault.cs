using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using HamerSoft.LastPassUnity.Core.CLI.Osx;
using HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu;
using HamerSoft.Threading;
using LastPass;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core
{
    internal class Vault : IDisposable
    {
        private global::LastPass.Vault _vault;
        private Account[] _accounts;
        private Config _config;
        private List<Tuple<string, string>> _suggestions;

        internal Vault()
        {
        }

        internal void Login(Credentials credentials, OtpUi otpUi, Action<bool, Exception> callback)
        {
            if (_config && _config.UseCli)
                switch (Application.platform)
                {
                    case RuntimePlatform.OSXEditor:
                    case RuntimePlatform.OSXPlayer:
                    case RuntimePlatform.WindowsPlayer:
                    case RuntimePlatform.WindowsEditor:
                    case RuntimePlatform.LinuxPlayer:
                    case RuntimePlatform.LinuxEditor:
                        LoginCli(credentials, otpUi, callback);
                        break;
#if LP_ANDROID
                    case RuntimePlatform.Android:
#endif
#if LP_IOS
                    case RuntimePlatform.IPhonePlayer:
#endif
                        LoginLastPassSharp(credentials, otpUi, callback);
                        break;
                }
            else
                LoginLastPassSharp(credentials, otpUi, callback);
        }

        private void LoginLastPassSharp(Credentials credentials, OtpUi otpUi, Action<bool, Exception> callback)
        {
            Dispatcher.RunAsync(() =>
            {
                global::LastPass.Vault.Open(
                    credentials.Username,
                    credentials.Password,
                    new ClientInfo(GetPlatform(), credentials.Id, credentials.Description,
                        credentials.IsTrustedDevice),
                    otpUi,
                    vault =>
                    {
                        Dispatcher.ToMainThread(() =>
                        {
                            _vault = vault;
                            _config.AddFilter(credentials.Username);
                            _suggestions = GenerateSuggestions();
                            callback?.Invoke(true, null);
                        });
                    },
                    () =>
                    {
                        Dispatcher.ToMainThread(() =>
                        {
                            callback?.Invoke(false,
                                new LoginException(
                                    LoginException.FailureReason.LastPassIncorrectGoogleAuthenticatorCode,
                                    "Login unsuccessful, OTP Cancelled!"));
                        });
                    });
            }, exception => { callback?.Invoke(false, exception); });
        }


        private void LoginCli(Credentials credentials, OtpUi otpUi, Action<bool, Exception> callback)
        {
            AbstractLastPassCli cli = LastPass.PerformStandAloneOrEditorAction<AbstractLastPassCli>(
                () => new OsxLastPassCli(),
                () => new WindowsUbuntuLastPassCli(),
                () => new LinuxLastPassCli());

            void InternalCallback(bool success, Exception exception)
            {
                if (success)
                {
                    _config.AddFilter(credentials.Username);
                    _accounts = ParseCsv(cli.Export(), out exception);
                    _suggestions = GenerateSuggestions();
                    cli.Logout();
                    callback.Invoke(_accounts != null, exception);
                }
                else
                    callback.Invoke(false, exception);

                _accounts = null;
                cli.Dispose();
            }

            cli.Login(credentials, otpUi, InternalCallback);
        }

        private Account[] ParseCsv(string csv, out Exception exception)
        {
            try
            {
                StringReader reader = new StringReader(csv);
                string line = reader.ReadLine();
                List<Account> accounts = new List<Account>();
                while ((line = reader.ReadLine()) != null)
                {
                    string[] separated = line.Split(',');
                    accounts.Add(new Account(
                        System.Guid.NewGuid().ToString(),
                        separated[4],
                        separated[1],
                        separated[2],
                        separated[0],
                        separated[5]
                    ));
                }

                exception = null;
                return accounts.ToArray();
            }
            catch (Exception e)
            {
                exception = e;
                return null;
            }
        }

        private Platform GetPlatform()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer:
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.LinuxPlayer:
                case RuntimePlatform.LinuxEditor:
                    return Platform.Desktop;
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    return Platform.Mobile;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        internal void UpdateConfig(Config config)
        {
            _config = config;
            _suggestions = GenerateSuggestions();
        }

        private List<Tuple<string, string>> GenerateSuggestions()
        {
            if (_vault != null)
                return _vault.Accounts
                    ?.Where(HasAccountMatch)
                    ?.Select(a => new Tuple<string, string>(a.Username, a.Password))
                    ?.ToList() ?? new List<Tuple<string, string>>();
            else if (_accounts != null)
            {
                return _accounts.Where(HasAccountMatch)
                    ?.Select(a => new Tuple<string, string>(a.Username, a.Password))
                    ?.ToList();
            }

            return new List<Tuple<string, string>>();
        }

        private bool HasAccountMatch(global::LastPass.Account account)
        {
            bool HasMatch(string value)
            {
                return _config.Filters.ToList().Exists(value.Contains);
            }

            return HasMatch(account.Group) || HasMatch(account.Id) || HasMatch(account.Name) || HasMatch(account.Url) ||
                   HasMatch(account.Username);
        }

        private bool HasAccountMatch(Account account)
        {
            bool HasMatch(string value)
            {
                return _config.Filters.ToList().Exists(value.Contains);
            }

            return HasMatch(account.Group) || HasMatch(account.Id) || HasMatch(account.Name) || HasMatch(account.Url) ||
                   HasMatch(account.Username);
        }

        internal List<Tuple<string, string>> GetSuggestions()
        {
            return _suggestions;
        }

        public void UpdateAccounts(Credentials credentials, OtpUi otpUi, Action<bool, Exception> callback)
        {
            Login(credentials, otpUi, callback);
        }

        public void Dispose()
        {
            _suggestions = null;
            _accounts = null;
            _vault = null;
        }
    }
}