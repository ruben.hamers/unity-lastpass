using System;
using CredentialManagement;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using HamerSoft.LastPassUnity.Core.CLI.Osx;
#if LP_ANDROID
using AndroidPwm = HamerSoft.LastPassUnity.Core.Mobile.Android.PasswordManager;
#endif
#if LP_IOS
using IosPwm = HamerSoft.LastPassUnity.Core.Mobile.IOS.PasswordManager;
#endif
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core
{
    internal class Deleter : MasterPasswordDelegator<bool>
    {
        private Credentials _credentials;

        public Deleter(Credentials credentials, string passwordId) : base(passwordId)
        {
            _credentials = credentials;
        }

        protected override bool DelegateLinux()
        {
            var linuxKeyring = new LinuxKeyring(LastPass.KEYRING_SYSTEM);
            linuxKeyring.DeletePassword(_credentials);
            return linuxKeyring.GetPassword() == null;
        }

#if LP_ANDROID
        protected override bool DelegateAndroid()
        {
            using (var pwm = new AndroidPwm(LastPass.KEYRING_SYSTEM, ANDROID_SHARED_PREFERENCES_KEY))
                return pwm.DeletePassword();
        }
#endif
#if LP_IOS
        protected override bool DelegateIos()
        {
            var pwm = new IosPwm();
            return pwm.DeletePassword();
        }
#endif
        protected override bool DelegateWindows()
        {
            try
            {
                using (var c = new Credential {Target = _passwordId})
                {
                    if (!c.Exists())
                        return false;
                    c.Delete();
                    return true;
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
                return false;
            }
        }

        protected override bool DelegateOsx()
        {
            var osxKeyring = new OsxKeyring(LastPass.KEYRING_SYSTEM);
            osxKeyring.DeletePassword(_credentials);
            return osxKeyring.GetPassword() == null;
        }

        public override void Dispose()
        {
            base.Dispose();
            _credentials = null;
        }
    }
}