using System;
using CredentialManagement;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using HamerSoft.LastPassUnity.Core.CLI.Osx;

#if LP_ANDROID
using AndroidPwm = HamerSoft.LastPassUnity.Core.Mobile.Android.PasswordManager;
#endif
#if LP_IOS
using IosPwm = HamerSoft.LastPassUnity.Core.Mobile.IOS.PasswordManager;
#endif

namespace HamerSoft.LastPassUnity.Core
{
    internal class Persister : MasterPasswordDelegator<bool>
    {
        private Credentials _credentials;

        public Persister(Credentials credentials, string passwordId) : base(passwordId)
        {
            _credentials = credentials;
        }

        protected override bool DelegateLinux()
        {
            var linuxKeyring = new LinuxKeyring(LastPass.KEYRING_SYSTEM);
            linuxKeyring.AddPassword(_credentials);
            return linuxKeyring.GetPassword() != null;
        }
#if LP_ANDROID
        protected override bool DelegateAndroid()
        {
            using (var pwm = new AndroidPwm(LastPass.KEYRING_SYSTEM, ANDROID_SHARED_PREFERENCES_KEY))
                return pwm.SavePassword(_credentials.Join());
        }
#endif
#if LP_IOS
        protected override bool DelegateIos()
        {
            var pwm = new IosPwm();
            return pwm.SavePassword(_credentials.Join());
        }
#endif

        protected override bool DelegateWindows()
        {
            try
            {
                using (var cred = new Credential())
                {
                    cred.Password = _credentials.Password;
                    cred.Target = _passwordId;
                    cred.Type = CredentialType.Generic;
                    cred.Username = _credentials.Username;
                    cred.PersistanceType = PersistanceType.LocalComputer;
                    cred.Save();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected override bool DelegateOsx()
        {
            var osxKeyring = new OsxKeyring(LastPass.KEYRING_SYSTEM);
            osxKeyring.AddPassword(_credentials);
            return osxKeyring.GetPassword() != null;
        }

        public override void Dispose()
        {
            base.Dispose();
            _credentials = null;
        }
    }
}