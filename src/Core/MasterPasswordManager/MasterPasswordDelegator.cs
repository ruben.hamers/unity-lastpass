using System;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core
{
    internal abstract class MasterPasswordDelegator<T> : IDisposable
    {
        protected readonly string _passwordId;
        protected const string ANDROID_SHARED_PREFERENCES_KEY = "Unity-LastPass-pref";

        internal MasterPasswordDelegator(string passwordId)
        {
            _passwordId = passwordId;
        }

        internal T Delegate()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer:
                    return DelegateOsx();
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.WindowsEditor:
                    return DelegateWindows();
#if LP_IOS
                case RuntimePlatform.IPhonePlayer:
                    return DelegateIos();
#endif
#if LP_ANDROID
                case RuntimePlatform.Android:
                    return DelegateAndroid();
#endif
                case RuntimePlatform.LinuxPlayer:
                case RuntimePlatform.LinuxEditor:
                    return DelegateLinux();
                default:
                    Debug.LogWarning($"Unsupported platform: {Application.platform} for LastPass!");
                    return default;
            }
        }

        protected abstract T DelegateLinux();
#if LP_ANDROID
        protected abstract T DelegateAndroid();
#endif
#if LP_IOS
        protected abstract T DelegateIos();
#endif
        protected abstract T DelegateWindows();
        protected abstract T DelegateOsx();

        public virtual void Dispose()
        {
        }
    }
}