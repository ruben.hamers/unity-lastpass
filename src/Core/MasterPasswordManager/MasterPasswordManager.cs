using System;

namespace HamerSoft.LastPassUnity.Core
{
    internal class MasterPasswordManager : IDisposable
    {
        private Credentials _credentials;
        private const string PASSWORD_ID = "UnityLastPass";

        internal MasterPasswordManager(Credentials credentials)
        {
            _credentials = credentials;
        }

        internal MasterPasswordManager()
        {
        }

        internal bool Save(Credentials credentials = null)
        {
            if (credentials != null)
                _credentials = credentials;
            using (Persister p = new Persister(_credentials, PASSWORD_ID))
                return p.Delegate();
        }

        internal Credentials Load()
        {
            using (Loader l = new Loader(PASSWORD_ID))
                return _credentials = l.Delegate();
        }

        internal bool Delete()
        {
            using (Deleter d = new Deleter(_credentials, PASSWORD_ID))
                return d.Delegate();
        }

        public void Dispose()
        {
            _credentials = null;
        }
    }
}