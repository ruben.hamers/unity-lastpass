#if LP_IOS
using IosKeyChain;

namespace HamerSoft.LastPassUnity.Core.Mobile.IOS
{
    public class PasswordManager : KeyChain
    {
        public bool SavePassword(string password)
        {
            SetString(LastPass.KEYRING_SYSTEM, password);
            return !string.IsNullOrEmpty(GetPassword());
        }

        public bool DeletePassword()
        {
           DeleteKey(LastPass.KEYRING_SYSTEM);
           return string.IsNullOrEmpty(GetPassword());
        }

        public string GetPassword()
        {
            return GetString(LastPass.KEYRING_SYSTEM);
        }
    }
}
#endif