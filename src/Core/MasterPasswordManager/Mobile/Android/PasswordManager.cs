#if LP_ANDROID
using HamerSoft.AndroidKeyChain;

namespace HamerSoft.LastPassUnity.Core.Mobile.Android
{
    public class PasswordManager : KeyChain
    {
        public PasswordManager(string alias, string sharedPreferencesKey) : base(alias, sharedPreferencesKey)
        {
        }

        public bool SavePassword(string password)
        {
            return PasswordManager.SavePassword(password);
        }

        public bool DeletePassword()
        {
            return PasswordManager.DeletePassword();
        }

        public string GetPassword()
        {
            return PasswordManager.GetPassword();
        }
    }
}
#endif