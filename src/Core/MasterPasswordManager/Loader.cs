using System;
using CredentialManagement;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using HamerSoft.LastPassUnity.Core.CLI.Osx;

#if LP_ANDROID
using AndroidPwm = HamerSoft.LastPassUnity.Core.Mobile.Android.PasswordManager;
#endif
#if LP_IOS
using IosPwm = HamerSoft.LastPassUnity.Core.Mobile.IOS.PasswordManager;
#endif

namespace HamerSoft.LastPassUnity.Core
{
    internal class Loader : MasterPasswordDelegator<Credentials>
    {
        public Loader(string passwordId) : base(passwordId)
        {
        }

        protected override Credentials DelegateLinux()
        {
            return new LinuxKeyring(LastPass.KEYRING_SYSTEM).GetPassword();
        }
#if LP_ANDROID
        protected override Credentials DelegateAndroid()
        {
            using (var pwm = new AndroidPwm(LastPass.KEYRING_SYSTEM, ANDROID_SHARED_PREFERENCES_KEY))
                return Credentials.Disjoin(pwm.GetPassword());
        }
#endif
#if LP_IOS
        protected override Credentials DelegateIos()
        {
            var pwm = new IosPwm();
            return Credentials.Disjoin(pwm.GetPassword());
        }
#endif
        protected override Credentials DelegateWindows()
        {
            using (var cred = new Credential())
            {
                cred.Target = _passwordId;
                cred.Load();
                if (cred.Exists() && !string.IsNullOrEmpty(cred.Username) && !string.IsNullOrEmpty(cred.Password))
                    return new Credentials(cred.Username, cred.Password);
                else
                    return null;
            }
        }

        protected override Credentials DelegateOsx()
        {
            return new OsxKeyring(LastPass.KEYRING_SYSTEM).GetPassword();
        }
    }
}