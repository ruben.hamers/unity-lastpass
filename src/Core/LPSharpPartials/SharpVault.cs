using System;
using System.Collections.Specialized;

/*
 * It sucks I have to do this but I need a callback based login option
 * We can not block the main thread or Unity will not respond anymore
 * So I made Vault, Fetcher and Ui partial in order to implement a callback login feature. (Also made Session public)
 */
namespace LastPass
{
    public partial class Vault
    {
        public static void Open(string username, string password, ClientInfo clientInfo, Ui ui, Action<Vault> callback,
            Action cancelledCallback)
        {
            if (callback == null)
                return;
            Download(username,
                password,
                clientInfo,
                ui,
                blob => { callback?.Invoke(Create(blob, username, password)); },
                cancelledCallback);
        }

        private static void Download(string username, string password, ClientInfo clientInfo, Ui ui,
            Action<Blob> callback, Action cancelledCallback)
        {
            if (callback == null)
                return;
            Fetcher.Login(username,
                password,
                clientInfo,
                ui,
                session =>
                {
                    try
                    {
                        callback?.Invoke(Fetcher.Fetch(session));
                    }
                    finally
                    {
                        Fetcher.Logout(session);
                    }
                },
                cancelledCallback);
        }
    }

    public partial class Fetcher
    {
        internal static void Login(string username, string password, ClientInfo clientInfo, Ui ui,
            Action<Session> callback, Action cancelledCallback)
        {
            using (var webClient = new WebClient())
                Login(username, password, clientInfo, ui, webClient, callback, cancelledCallback);
        }

        // TODO: Write tests for this. Possibly the whole current concept of how it's tested
        //       should be rethought. Maybe should simply tests against a fake server.
        internal static void Login(string username, string password, ClientInfo clientInfo, Ui ui, IWebClient webClient,
            Action<Session> callback, Action cancelledCallback)
        {
            // 1. First we need to request PBKDF2 key iteration count.
            var keyIterationCount = RequestIterationCount(username, webClient);

            // 2. Knowing the iterations count we can hash the password and log in.
            //    One the first attempt simply with the username and password.
            var response = PerformSingleLoginRequest(username,
                password,
                keyIterationCount,
                new NameValueCollection(),
                clientInfo,
                webClient);
            var session = ExtractSessionFromLoginResponse(response, keyIterationCount, clientInfo);
            if (session != null)
            {
                callback.Invoke(session);
                return;
            }

            // 3. The simple login failed. This is usually due to some error, invalid credentials or
            //    a multifactor authentication being enabled.
            var cause = GetOptionalErrorAttribute(response, "cause");

            // 3.1. One-time-password is required
            if (KnownOtpMethods.ContainsKey(cause))
            {
                LoginWithOtp(username,
                    password,
                    keyIterationCount,
                    KnownOtpMethods[cause],
                    clientInfo,
                    ui,
                    webClient, newSession =>
                    {
                        if (newSession == null)
                            throw CreateLoginException(response);

                        // 4. The login with OTP or OOB is successful. Tell the server to trust this device next time.
                        if (clientInfo.TrustThisDevice)
                            Trust(newSession, clientInfo, webClient);

                        callback.Invoke(newSession);
                    },
                    cancelledCallback);
                return;
            }

            // 3.2. Some out-of-bound authentication is enabled. This does not require any
            //      additional input from the user.
            else if (cause == "outofbandrequired")
                session = LoginWithOob(username,
                    password,
                    keyIterationCount,
                    ExtractOobMethodFromLoginResponse(response),
                    clientInfo,
                    ui,
                    webClient);

            if (session == null)
                throw CreateLoginException(response);

            // 4. The login with OTP or OOB is successful. Tell the server to trust this device next time.
            if (clientInfo.TrustThisDevice)
                Trust(session, clientInfo, webClient);

            callback.Invoke(session);
        }

        private static void LoginWithOtp(string username,
            string password,
            int keyIterationCount,
            Ui.SecondFactorMethod method,
            ClientInfo clientInfo,
            Ui ui,
            IWebClient webClient,
            Action<Session> callback,
            Action cancelledCallback)
        {
            ui.ProvideSecondFactorPasswordAsync(method, otp =>
                {
                    var response = PerformSingleLoginRequest(username,
                        password,
                        keyIterationCount,
                        new NameValueCollection {{"otp", otp}},
                        clientInfo,
                        webClient);
                    var session = ExtractSessionFromLoginResponse(response, keyIterationCount, clientInfo);
                    if (session != null)
                        callback?.Invoke(session);
                    else
                        throw CreateLoginException(response);
                },
                cancelledCallback);
        }
    }

    public abstract partial class Ui
    {
        public abstract void ProvideSecondFactorPasswordAsync(SecondFactorMethod method, Action<string> callback,
            Action cancelledCallback);
        public abstract void ProvideSecondFactorPassword(SecondFactorMethod method, Action<string> callback,
            Action cancelledCallback);
    }
}