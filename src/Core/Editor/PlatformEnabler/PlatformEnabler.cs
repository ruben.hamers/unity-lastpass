using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor.PlatformEnabler
{
    public class PlatformEnabler : IDisposable
    {
        private BuildTargetGroup _platform;
        private string _ifdef;
        private readonly string _pluginDirectory;
        private string _currentDefines;

        public PlatformEnabler(BuildTargetGroup platform, string ifdef, string pluginDirectory)
        {
            _platform = platform;
            _ifdef = ifdef;
            _pluginDirectory = pluginDirectory;
            _currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(_platform);
        }

        public void Enable()
        {
            EnableIfDef();
            if (TryFindDirectory($"{_pluginDirectory}~", out var path))
                Directory.Move(path, path.TrimEnd('~'));
            else if (!TryFindDirectory($"{_pluginDirectory}", out path))
                Debug.LogError($"Cannot find {_pluginDirectory} directory for {_ifdef} Define Symbol");

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        internal void EnableIfDef()
        {
            _currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(_platform);
            if (!_currentDefines.Contains(_ifdef))
                if (string.IsNullOrEmpty(_currentDefines))
                    PlayerSettings.SetScriptingDefineSymbolsForGroup(_platform, $"{_ifdef};");
                else
                    PlayerSettings.SetScriptingDefineSymbolsForGroup(_platform, $"{_currentDefines};{_ifdef};");
        }

        internal bool TryFindDirectory(string name, out string dir)
        {
            string[] result = Directory.GetDirectories(Application.dataPath, name, SearchOption.AllDirectories);
            dir = result.Length > 0 ? result[0] : null;
            return result.Length > 0;
        }

        public void Disable()
        {
            DisableIfDef();
            if (TryFindDirectory($"{_pluginDirectory}", out var path))
                Directory.Move(path, $@"{path}~");
            else if (!TryFindDirectory($"{_pluginDirectory}~", out path))
                Debug.LogError($"Cannot find {_pluginDirectory} directory for {_ifdef} Define Symbol");

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        internal void DisableIfDef()
        {
            _currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(_platform);
            if (_currentDefines.Contains($"{_ifdef};"))
                PlayerSettings.SetScriptingDefineSymbolsForGroup(_platform, _currentDefines.Replace($"{_ifdef};", ""));
            if (_currentDefines.Contains($"{_ifdef}"))
                PlayerSettings.SetScriptingDefineSymbolsForGroup(_platform, _currentDefines.Replace($"{_ifdef}", ""));
        }

        public void Dispose()
        {
            _ifdef = null;
            _currentDefines = null;
        }
    }
}