using System;
using UnityEngine;
using HamerSoft.Threading;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal class EditorOtpUi : OtpUi
    {
        public override void ProvideSecondFactorPasswordAsync(SecondFactorMethod method, Action<string> callback, Action cancelledCallback)
        {
            Dispatcher.ToMainThread(() => { EditorOtpWindow.ShowOtp(method, s => { Dispatcher.RunAsync(() => { callback.Invoke(s); }); }, cancelledCallback); });
        }

        public override void ProvideSecondFactorPassword(SecondFactorMethod method, Action<string> callback, Action cancelledCallback)
        {
            EditorOtpWindow.ShowOtp(method, callback, cancelledCallback);
        }

        public override string ProvideSecondFactorPassword(SecondFactorMethod method)
        {
            throw new NotImplementedException();
        }

        public override void AskToApproveOutOfBand(OutOfBandMethod method)
        {
            Debug.LogWarning("Ask Out of Band!");
        }

        public override void ShowWarning(string message)
        {
            EditorOtpWindow.ShowWarning(message);
        }
    }
}