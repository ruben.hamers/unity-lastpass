using System;
using LastPass;
using UnityEngine;
using UnityEditor;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    public class EditorOtpWindow : EditorWindow
    {
        private const int WIDTH = 250;
        private const int HEIGHT = 70;
        private string _passCode;
        private static Action<string> _callback;
        private static global::LastPass.Ui.SecondFactorMethod _method;
        private static string _warning;
        private static Action _cancelledCallback;
        
        internal static void ShowOtp(global::LastPass.Ui.SecondFactorMethod method, Action<string> callback, Action cancelledCallback)
        {
            
            _cancelledCallback = cancelledCallback;
            _method = method;
            _callback = callback;
            _warning = null;
            SpawnWindow();
        }

        private static void SpawnWindow()
        {
            EditorOtpWindow window = CreateInstance<EditorOtpWindow>();
            window.position = new Rect(
                Screen.currentResolution.width / 2 - WIDTH / 2,
                Screen.currentResolution.height / 2 - HEIGHT / 2,
                WIDTH,
                HEIGHT);
            window.ShowPopup();
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Please enter your one-time-passcode:", EditorStyles.wordWrappedLabel);
            if (!string.IsNullOrEmpty(_warning))
                EditorGUILayout.HelpBox(_warning, MessageType.Warning);
            _passCode = EditorGUILayout.TextField("OTP", _passCode);
            DrawButtons();
        }

        private void DrawButtons()
        {
            EditorGUILayout.BeginHorizontal();
            DrawConfirmButton();
            DrawCancelButton();
            EditorGUILayout.EndHorizontal();
        }

        private void DrawCancelButton()
        {
            if (GUILayout.Button("Cancel"))
            {
                _warning = null;
                Close();
                _cancelledCallback?.Invoke();
            }
        }

        private void DrawConfirmButton()
        {
            if (GUILayout.Button("Confirm"))
            {
                _warning = null;
                try
                {
                    Close();
                    _callback?.Invoke(_passCode);
                }
                catch (LoginException loginException)
                {
                    if (loginException.Reason == LoginException.FailureReason.LastPassIncorrectGoogleAuthenticatorCode
                        ||
                        loginException.Reason == LoginException.FailureReason.LastPassIncorrectYubikeyPassword)
                    {
                        ShowWarning("Incorrect OTP!");
                        Debug.LogError("The provided one-time-passcode is incorrect! Please enter a correct OTP.");
                    }
                    else
                        throw;
                }
            }
        }

        public static void ShowWarning(string message)
        {
            _warning = message;
        }
    }
}