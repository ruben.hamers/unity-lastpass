using System.Collections.Generic;
using System.Linq;
#if LP_IOS
using IosKeyChain;
#endif
using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    public class LastPassConfig : EditorWindow
    {
        private static DrawableSection[] _drawables;
        private static LastPassConfig _window;
        private static string _configPath = "Resources/LastPass";

        [MenuItem("HamerSoft/LastPass/Configuration")]
        private static void ShowWindow()
        {
#if LP_IOS
            KeyChain.Initialize();
#endif
            _window = GetWindow<LastPassConfig>();
            _window.titleContent = new GUIContent("LastPass Config");
            _window.BuildDrawers();
            _window.Show();
        }

        private void BuildDrawers()
        {
            UnAssignEvent();
            var drawers = new List<DrawableSection>()
            {
                new ConfigDrawer(_configPath),
                new LoginDrawer()
            };

            LastPass.PerformStandAloneOrEditorAction(
                () => drawers.InsertRange(0, new DrawableSection[] {new OsxBrewInstallerDrawer(), new OsxLastPassInstaller()}),
                () => drawers.InsertRange(0, new DrawableSection[] {new WindowsUbuntuLastPassCliInstaller()}),
                () => drawers.InsertRange(0, new DrawableSection[] {new LinuxKeyringDrawer(), new LinuxLassPastInstaller()}));

            _drawables = drawers.ToArray();
            GetDrawer<LoginDrawer>().LoggedOut += OnLoggedOut_Handler;
        }

        private void UnAssignEvent()
        {
            if (GetDrawer<LoginDrawer>() is LoginDrawer ld)
                ld.LoggedOut -= OnLoggedOut_Handler;
        }

        private void OnDestroy()
        {
            UnAssignEvent();
        }

        private void OnGUI()
        {
            if (_drawables == null)
                return;
            foreach (DrawableSection drawable in _drawables)
                drawable.Draw();
        }

        internal static T GetDrawer<T>() where T : class
        {
            return _drawables?.FirstOrDefault(d => d is T) as T;
        }

        private void OnLoggedOut_Handler()
        {
            UnAssignEvent();
            _window.Close();
        }
    }
}