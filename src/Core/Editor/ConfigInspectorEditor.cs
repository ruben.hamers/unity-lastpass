using System;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    [CustomEditor(typeof(Config), true)]
    public class ConfigInspectorEditor : UnityEditor.Editor
    {
        private const string ANDROID_KEYCHAIN_DIR = "AndroidKeyChain";
        private const string IOS_KEYCHAIN_DIR = "IOSKeyChain";

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var config = (Config) target;

            if (config.AndroidSupported)
                DrawButton("Disable Android Support",
                    () => DisableSupport(BuildTargetGroup.Android, LastPass.ANDROID_PLATFORM,
                        ANDROID_KEYCHAIN_DIR, ref config.AndroidSupported));
            else
                DrawButton("Enable Android Support",
                    () => EnableSupport(BuildTargetGroup.Android, LastPass.ANDROID_PLATFORM,
                        ANDROID_KEYCHAIN_DIR, ref config.AndroidSupported));

            if (config.IosSupported)
                DrawButton("Disable iOS Support",
                    () => DisableSupport(BuildTargetGroup.iOS, LastPass.IOS_PLATFORM,
                        IOS_KEYCHAIN_DIR, ref config.IosSupported));
            else
                DrawButton("Enable iOS Support",
                    () => EnableSupport(BuildTargetGroup.iOS, LastPass.IOS_PLATFORM,
                        IOS_KEYCHAIN_DIR, ref config.IosSupported));
        }

        private void DrawButton(string label, Action clicked)
        {
            if (GUILayout.Button(label))
            {
                clicked?.Invoke();
                EditorUtility.SetDirty(target);
            }
        }

        private void DisableSupport(BuildTargetGroup platform, string ifdef, string directory, ref bool flag)
        {
            using (var enabler = new PlatformEnabler.PlatformEnabler(platform, ifdef, directory))
            {
                enabler.Disable();
                flag = false;
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        private void EnableSupport(BuildTargetGroup platform, string ifdef, string directory, ref bool flag)
        {
            using (var enabler = new PlatformEnabler.PlatformEnabler(platform, ifdef, directory))
            {
                enabler.Enable();
                flag = true;
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
    }
}