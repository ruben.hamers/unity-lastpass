using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal class ConfigDrawer : DrawableSection<Config>
    {
        private readonly string _resourcesPath;

        public ConfigDrawer(string resourcesPath)
        {
            _resourcesPath = resourcesPath;
            Result = Config.Load();
        }

        internal override void Draw()
        {
            if (!LastPass.IsInstallComplete(false))
                return;

            EditorGUILayout.BeginVertical();
            EditorGUILayout.LabelField("Config");
            Result = EditorGUILayout.ObjectField(Result, typeof(Config), true) as Config;
            if (Result == null)
            {
                EditorGUILayout.HelpBox(
                    "There is no Config in Resources/LastPass/. You can create one with the button below.",
                    MessageType.Info);
                if (GUILayout.Button("Create new Config"))
                    Selection.activeObject = Result = Config.Create(_resourcesPath);
            }

            EditorGUILayout.EndVertical();
        }

        
    }
}