using System;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal class LoginDrawer : DrawableSection<bool>
    {
        internal event Action LoggedOut;
        private string _username, _password;
        private bool _loggingIn, hasCredentials;
        private Config _config;

        public LoginDrawer()
        {
            hasCredentials = LastPass.HasCredentials;
        }

        internal override void Draw()
        {
            if (_loggingIn)
            {
                EditorGUILayout.HelpBox(
                    "Please wait while we are logging you in... This might take a while...",
                    MessageType.Info);
                return;
            }

            if (!LastPass.IsInstallComplete(false) || _loggingIn)
                return;

            if (LastPass.IsLoggedIn)
            {
                EditorGUILayout.HelpBox("☑ You are logged in!", MessageType.Info);
                DrawLogout();
                return;
            }

            if (hasCredentials)
                Authenticate();
            else
                DrawLogin();
        }

        private void DrawLogout()
        {
            if (GUILayout.Button("Logout"))
            {
                LastPass.Logout();
                OnLoggedOut();
            }
        }

        private void Authenticate()
        {
            _loggingIn = true;
            GetConfig();
            LastPass.Login(_config, new EditorOtpUi(), ProcessLogin);
        }

        private void GetConfig()
        {
            if (!_config)
                _config = Config.Load();
        }

        private void DrawLogin()
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.LabelField("Login with your LastPass Master email and password:");
            _username = EditorGUILayout.TextField("UserName", _username);
            _password = EditorGUILayout.PasswordField("Password", _password);
            if (GUILayout.Button("Login"))
                Login();
            EditorGUILayout.EndVertical();
        }

        private void Login()
        {
            GetConfig();
            _loggingIn = true;
            var credentials = new Credentials(_username, _password);
            credentials.SetIsTrustedDevice(_config.TrustThisDevice);
            LastPass.Login(credentials, new EditorOtpUi(), ProcessLogin);
        }

        private void ProcessLogin(bool success, Exception exception)
        {
            Debug.Log($"Login {(success ? "successful" : "failed!")}");
            _loggingIn = false;
            if (success)
                hasCredentials = true;
        }

        protected void OnLoggedOut()
        {
            LoggedOut?.Invoke();
        }
    }
}