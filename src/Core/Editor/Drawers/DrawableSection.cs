namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal abstract class DrawableSection
    {
        internal abstract void Draw();
    }

    internal abstract class DrawableSection<T> : DrawableSection
    {
        protected T Result;

        internal virtual T GetDrawable()
        {
            return Result;
        }
    }
}