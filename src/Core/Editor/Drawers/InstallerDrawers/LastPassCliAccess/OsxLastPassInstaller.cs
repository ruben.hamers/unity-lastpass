using HamerSoft.LastPassUnity.Core.CLI;
using HamerSoft.LastPassUnity.Core.CLI.Osx;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal class OsxLastPassInstaller : LastPassInstaller
    {
        protected override ICLI CreateCli()
        {
            return new OsxLastPassCli();
        }
    }
}