using HamerSoft.LastPassUnity.Core.CLI;
using HamerSoft.LastPassUnity.Core.CLI.Linux;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal class LinuxLassPastInstaller : LastPassInstaller
    {
        protected override ICLI CreateCli()
        {
            return new LinuxLastPassCli();
        }
    }
}