﻿using HamerSoft.LastPassUnity.Core.CLI;
using HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal class WindowsUbuntuLastPassCliInstaller : LastPassInstaller
    {
        private bool _isUbuntuInstalled;

        public WindowsUbuntuLastPassCliInstaller()
        {
            CheckWinbuntu();
        }

        protected override ICLI CreateCli()
        {
            return new WindowsUbuntuLastPassCli();
        }

        internal override void Draw()
        {
            if (_isUbuntuInstalled)
                base.Draw();
            else
            {
                GUILayout.Label("Please install the Ubuntu subsystem on your windows device! /r/n Please refer to the README if you need help.");
                if (GUILayout.Button($"Verify Windows Ubuntu Subsystem"))
                    CheckWinbuntu();
                GUILayout.Space(5);
                if (GUILayout.Button($"Open Install link"))
                    Application.OpenURL("https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab");
            }
        }

        private void CheckWinbuntu()
        {
            try
            {
                Cli.RunCommand("ls -l");
                _isUbuntuInstalled = true;
            }
            catch (CliException e)
            {
                Debug.LogError(e.StandardError);
                _isUbuntuInstalled = false;
            }
        }
    }
}