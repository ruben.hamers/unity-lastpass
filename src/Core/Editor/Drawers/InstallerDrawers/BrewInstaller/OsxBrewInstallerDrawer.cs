﻿using HamerSoft.LastPassUnity.Core.CLI;
using HamerSoft.LastPassUnity.Core.CLI.Osx;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal class OsxBrewInstallerDrawer : InstallerDrawer
    {
        private string _sudo;

        internal override void Draw()
        {
            if (IsInstalling)
            {
                EditorGUILayout.HelpBox(
                    $"Please wait while we are installing {GetName()}, note this may take a while...",
                    MessageType.Info);
                return;
            }else if (IsInstallComplete())
            {
                EditorGUILayout.HelpBox(
                    $"Install: {GetName()} complete.",
                    MessageType.Info);
                return;
            }
            
            _sudo = EditorGUILayout.PasswordField("Please enter your sudo password:", _sudo);
            if (GUILayout.Button($"Install {GetName()}"))
            {
                IsInstalling = TriedInstall = true;
                Install(_sudo);
            }
            else
                EditorGUILayout.HelpBox(
                    "No sudo password entered.",
                    MessageType.Info);
        }

        protected override string GetName()
        {
            return "HomeBrew";
        }

        protected override ICLI CreateCli()
        {
            return new OsxBrewCli();
        }
    }
}