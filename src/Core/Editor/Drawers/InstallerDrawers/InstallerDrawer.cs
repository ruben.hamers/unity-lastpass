using System;
using HamerSoft.LastPassUnity.Core.CLI;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal abstract class InstallerDrawer : DrawableSection<bool>, IDisposable
    {
        protected bool IsInstalling, TriedInstall;
        protected ICLI Cli;
        protected abstract string GetName();

        public InstallerDrawer()
        {
            Cli = CreateCli();
            Cli.IsInstalled(true);
        }

        protected abstract ICLI CreateCli();

        protected virtual bool IsInstallComplete()
        {
            return Cli.IsInstalled(TriedInstall);
        }

        protected virtual void Install(string sudo)
        {
            Cli.Install(sudo, InstallComplete);
        }

        protected void InstallComplete(bool success)
        {
            IsInstalling = TriedInstall = false;

            Debug.Log($"{GetName()} install {(success ? "complete" : "failed")}!");
            Dispose();
        }

        public virtual void Dispose()
        {
          
        }
    }
}