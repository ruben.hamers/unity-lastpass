using HamerSoft.LastPassUnity.Core.CLI;
using HamerSoft.LastPassUnity.Core.CLI.Linux;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal class LinuxKeyringDrawer : KeyringInstaller
    {
        protected override ICLI CreateCli()
        {
            return new LinuxKeyring(LastPass.KEYRING_SYSTEM);
        }
    }
}