using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Core.Editor
{
    internal abstract class KeyringInstaller : InstallerDrawer
    {
        private string _sudo;
        
        protected override string GetName()
        {
            return "Python Keyring";
        }
        
        internal override void Draw()
        {
            if (IsInstalling)
            {
                EditorGUILayout.HelpBox(
                    $"Please wait while we are installing {GetName()}...",
                    MessageType.Info);
                return;
            }else if (IsInstallComplete())
            {
                EditorGUILayout.HelpBox(
                    $"Install: {GetName()} complete.",
                    MessageType.Info);
                return;
            }
            
            _sudo = EditorGUILayout.PasswordField("Please enter your sudo password:", _sudo);
            if (GUILayout.Button($"Install {GetName()}"))
            {
                IsInstalling = TriedInstall = true;
                Install(_sudo);
            }
            else
                EditorGUILayout.HelpBox(
                    "No sudo password entered.",
                    MessageType.Info);
        }

        public override void Dispose()
        {
            _sudo = null;
        }
    }
}