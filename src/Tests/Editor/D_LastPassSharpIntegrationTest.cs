﻿using System;
using LastPass;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Tests.LastPassSharp
{
    public class D_LastPassSharpIntegrationTest
    {
        private class TextUi : LastPass.Ui
        {
            public override void ProvideSecondFactorPasswordAsync(SecondFactorMethod method, Action<string> callback,Action cancelledCallback)
            {
                Debug.Log("this is not used in the integration tests since we do not walk that code path");
            }

            public override void ProvideSecondFactorPassword(SecondFactorMethod method, Action<string> callback, Action cancelledCallback)
            {
                Debug.Log("this is not used in the integration tests since we do not walk that code path");
            }

            public override string ProvideSecondFactorPassword(SecondFactorMethod method)
            {
                return GetAnswer($"Please enter {method} code");
            }

            public override void AskToApproveOutOfBand(OutOfBandMethod method)
            {
                Debug.Log($"Please approve out-of-band via {method}");
            }

            private static string GetAnswer(string prompt)
            {
                Debug.Log(prompt);
                Debug.Log("> ");
                var input = Console.ReadLine();

                return input == null ? "" : input.Trim();
            }
        }

        // A Test behaves as an ordinary method
        [Test]
        public void LastPassSharp_Integration_Test()
        {
            var username = TestingEnv.LpUsername;
            var password = TestingEnv.LpPassword;
            var id = "385e2742aefd399bd182c1ea4c1aac4b";
            var description = "Example for lastpass-sharp";

            try
            {
                // Fetch and create the vault from LastPass
                var vault = Vault.Open(username,
                    password,
                    new ClientInfo(Platform.Desktop, id, description, false),
                    new TextUi());

                // Dump all the accounts
                for (var i = 0; i < vault.Accounts.Length; ++i)
                {
                    var account = vault.Accounts[i];
                    Debug.Log(string.Format(@"{0}:\n" +
                                            "        id: {1}\n" +
                                            "      name: {2}\n" +
                                            "  username: {3}\n" +
                                            "  password: {4}\n" +
                                            "       url: {5}\n" +
                                            "     group: {6}\n",
                        i + 1,
                        account.Id,
                        account.Name,
                        account.Username,
                        account.Password,
                        account.Url,
                        account.Group));
                }

                Assert.NotNull(vault.Accounts);
                Assert.Greater(vault.Accounts.Length, 0);
            }
            catch (LoginException e)
            {
                Debug.Log($"Something went wrong: {e}");
            }
        }
    }
}