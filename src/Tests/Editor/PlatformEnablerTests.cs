using System.IO;
using HamerSoft.LastPassUnity.Core.Editor.PlatformEnabler;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Tests
{
    public class PlatformEnablerTests
    {
        private string _enabledPath;
        private string _disabledPath;

        [SetUp]
        public void Setup()
        {
            _enabledPath = Directory
                .CreateDirectory(Path.Combine(Application.dataPath, "UnitTesting")).FullName;
            _disabledPath =
                $"{Directory.CreateDirectory(Path.Combine(Application.dataPath, "UnitTestingDisabled")).FullName}~";
        }

        [Test]
        public void PlatformEnabler_Can_Disable_An_Enabled_Directory()
        {
            var enabler = new PlatformEnabler(BuildTargetGroup.Android, "Foo", "UnitTesting");
            enabler.Disable();
            Assert.True(enabler.TryFindDirectory($"UnitTesting~", out var dir));
        }

        [Test]
        public void PlatformEnabler_Can_Enable_A_Disabled_Directory()
        {
            var enabler = new PlatformEnabler(BuildTargetGroup.Android, "Foo", "UnitTestingDisabled");
            enabler.Enable();
            Assert.True(enabler.TryFindDirectory($"UnitTestingDisabled", out var dir));
            enabler.DisableIfDef();
        }

        [Test]
        public void PlatformEnabler_Can_Enable_An_IfDef()
        {
            var enabler = new PlatformEnabler(BuildTargetGroup.Android, "Foo", "UnitTestingDisabled~");
            enabler.EnableIfDef();
            Assert.True(PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Contains("Foo;") ||
                        PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Contains("Foo"));
            enabler.DisableIfDef();
        }

        [Test]
        public void PlatformEnabler_Can_Disable_An_IfDef()
        {
            var enabler = new PlatformEnabler(BuildTargetGroup.Android, "Foo", "UnitTestingDisabled~");
            enabler.EnableIfDef();
            Assert.True(PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Contains("Foo;") ||
                        PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Contains("Foo"));
            enabler.DisableIfDef();
            Assert.False(PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Contains("Foo;") ||
                         PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Contains("Foo"));
        }

        [TearDown]
        public void TearDown()
        {
            if (Directory.Exists(_enabledPath))
                Directory.Delete(_enabledPath);
            if (Directory.Exists($"{_enabledPath}~"))
                Directory.Delete($"{_enabledPath}~");
            if (Directory.Exists(_disabledPath))
                Directory.Delete(_disabledPath);
            if (Directory.Exists(_disabledPath.TrimEnd('~')))
                Directory.Delete(_disabledPath.TrimEnd('~'));
        }
    }
}