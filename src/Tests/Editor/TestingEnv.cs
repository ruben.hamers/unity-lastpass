using System;
using System.IO;
using UnityEngine;
using System.Linq;

namespace HamerSoft.LastPassUnity.Tests
{
    public static class TestingEnv
    {
        private class TestingConfig
        {
            public string LpUsername;
            public string LpPassword;
            public string LpOtpUserName;
            public string LpOtpPassword;
            public string OsPassword;
            public string WinBuntuOsPassword;
            public string MacOsPassword;
            public string KeyringSystem;
            public string ResourcesTestsPath;
            public string ConfigAssetName;
        }

        public static readonly string LpUsername = LoadConfig().LpUsername;
        public static readonly string LpPassword = LoadConfig().LpPassword;
        public static string LpOtpUsername = LoadConfig().LpOtpUserName;
        public static string LpOtpPassword = LoadConfig().LpOtpPassword;
        public static readonly string LinuxOsPassword = LoadConfig().OsPassword;
        public static readonly string WinBuntuOsPassword = LoadConfig().WinBuntuOsPassword;
        public static readonly string MacOsPassword = LoadConfig().MacOsPassword;
        public static readonly string KeyringSystem = LoadConfig().KeyringSystem;
        public static readonly string ResourcesTestsPath = LoadConfig().ResourcesTestsPath;
        public static readonly string AssetName = LoadConfig().ConfigAssetName;
        private static TestingConfig _config;
        private const string testConfigPath = "TestingConfig.json";

        public static bool InCiPipeline => System.Environment.GetCommandLineArgs().Contains("-nographics");

        private static TestingConfig LoadConfig()
        {
            if (_config != null)
                return _config;

            string path = Path.Combine(Application.dataPath, testConfigPath);
            if (!File.Exists(path))
                throw new NullReferenceException("No TestingConfig.json found in the root Assets folder!");

            using (StreamReader sr = new StreamReader(path))
                return _config = JsonUtility.FromJson<TestingConfig>(sr.ReadToEnd());
        }

        public static string GetCurrentOsPassword()
        {
            return Core.LastPass.PerformStandAloneOrEditorAction(
                () => MacOsPassword,
                () => WinBuntuOsPassword,
                () => LinuxOsPassword);
        }
    }
}