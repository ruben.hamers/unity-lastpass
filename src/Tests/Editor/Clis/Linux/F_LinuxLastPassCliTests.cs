using System;
using System.Reflection;
using HamerSoft.LastPassUnity.Core;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Tests
{
    public class F_LinuxLastPassCliTests
    {
        internal class MockLinuxLastPassCli : LinuxLastPassCli
        {
            internal void Login(Credentials credentials, OtpUi otpUi, Action<bool, Exception> internalCallback)
            {
                typeof(AbstractLastPassCli).GetMethod("Login", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, new object[] {credentials, otpUi, internalCallback});
            }

            internal void Logout()
            {
                typeof(AbstractLastPassCli)
                    .GetMethod("Logout", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, null);
            }

            internal string Export()
            {
                return (string) typeof(AbstractLastPassCli)
                    .GetMethod("Export", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, null);
            }
        }

        [Test,Order(0)]
        public void Test_Install()
        {
            var cli = new LinuxLastPassCli();
            cli.Install(TestingEnv.LinuxOsPassword, Assert.True);
        }

        [Test,Order(1)]
        public void Test_IsInstalled()
        {
            var cli = new LinuxLastPassCli();
            if (cli.IsInstalled(true))
                Assert.Pass();
            else
                cli.Install(TestingEnv.LinuxOsPassword, success => { Assert.True(cli.IsInstalled(true)); });
        }

        [Test,Order(2)]
        public void TestLogin()
        {
            var cli = new MockLinuxLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null,
                (success, exception) => { Assert.True(success); });
            cli.Logout();
        }

        [Test,Order(3)]
        public void TestLogout()
        {
            var cli = new MockLinuxLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null, (success, exception) =>
            {
                Assert.True(success);
                cli.Logout();
            });
        }

        [Test,Order(4)]
        public void Test_Export()
        {
            var cli = new MockLinuxLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null, (success, exception) =>
            {
                Assert.True(success);
                string export = cli.Export();
                Assert.True(!string.IsNullOrEmpty(export));
                cli.Logout();
            });
        }
    }
}