using System;
using System.Collections.Generic;
using System.Text;
using HamerSoft.LastPassUnity.Core.CLI;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using NUnit.Framework;

namespace HamerSoft.LastPassUnity.Tests
{
    public class A_CliCommandsTests
    {
        internal class CommandTestMock : AbstractLinuxCli
        {
            protected override string ProcessFileName => process;
            internal string process = "/bin/bash";

            public override bool IsInstalled(bool refresh = false)
            {
                throw new NotImplementedException();
            }

            public override void Install(string osPassword, Action<bool> success)
            {
                throw new NotImplementedException();
            }
        }

        private const string TEST_COMMAND = "[ \"lastpass\" = \"lastpass\" ]; echo $?";
        private CommandTestMock _commandTestMock;

        [SetUp]
        public void Setup()
        {
            _commandTestMock = new CommandTestMock();
        }

        [Test]
        public void HasCurrentUserRootAccess()
        {
            if (TestingEnv.InCiPipeline)
                Assert.Pass();
            else
                Assert.False(_commandTestMock.HasRootAccess(out var canSudo));
        }

        [Test]
        public void Run_Command_Test()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("0");
            Assert.AreEqual(sb.ToString(), _commandTestMock.RunCommand(TEST_COMMAND));
        }

        [Test]
        public void Run_Commands_Test()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("0");
            sb.AppendLine("0");
            Assert.AreEqual(sb.ToString(),
                _commandTestMock.RunCommand(new Queue<string>(new[] {TEST_COMMAND, TEST_COMMAND})));
        }

        [Test]
        public void ExceptionIsThrown_WhenProcessDoesNotExist()
        {
            _commandTestMock.process = "Foo";
            Assert.Throws<CliException>(() => { _commandTestMock.RunCommand("ls -l"); });
        }
    }
}