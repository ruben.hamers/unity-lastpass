using System;
using System.Reflection;
using HamerSoft.LastPassUnity.Core;
using HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Tests
{
    public class WindowsUbuntuLastPassCliTests
    {
        internal class MockWindowsUbuntuLastPassCli : WindowsUbuntuLastPassCli
        {
            internal void Login(Credentials credentials, OtpUi otpUi, Action<bool, Exception> internalCallback)
            {
                typeof(AbstractLastPassCli).GetMethod("Login", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, new object[] {credentials, otpUi, internalCallback});
            }

            internal void Logout()
            {
                typeof(AbstractLastPassCli)
                    .GetMethod("Logout", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, null);
            }

            internal string Export()
            {
                return (string) typeof(AbstractLastPassCli)
                    .GetMethod("Export", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, null);
            }
        }

        public bool IsWindows => Application.platform == RuntimePlatform.WindowsEditor ||
                                 Application.platform == RuntimePlatform.WindowsPlayer; 

        [Test]
        public void Test_IsInstalled()
        {
            if (!IsWindows)
            {
                Assert.Pass();
                return;
            }
            
            var cli = new WindowsUbuntuLastPassCli();
            if (cli.IsInstalled())
                Assert.Pass();
            else
                cli.Install(TestingEnv.WinBuntuOsPassword, success => { Assert.True(cli.IsInstalled()); });
        }

        [Test]
        public void Test_Install()
        {
            if (!IsWindows)
            {
                Assert.Pass();
                return;
            }
            var cli = new WindowsUbuntuLastPassCli();
            cli.Install(TestingEnv.WinBuntuOsPassword, Assert.True);
        }

        [Test]
        public void TestLogin()
        {
            if (!IsWindows)
            {
                Assert.Pass();
                return;
            }
            var cli = new MockWindowsUbuntuLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null,
                (success, exception) =>
                {
                    Assert.True(success);
                });
            cli.Logout();
        }

        [Test]
        public void TestLogout()
        {
            if (!IsWindows)
            {
                Assert.Pass();
                return;
            }
            var cli = new MockWindowsUbuntuLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null, (success, exception) =>
            {
                Assert.True(success);
                cli.Logout();
                Assert.Pass();
            });
        }

        [Test]
        public void Test_Export()
        {
            if (!IsWindows)
            {
                Assert.Pass();
                return;
            }
            var cli = new MockWindowsUbuntuLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null, (success, exception) =>
            {
                Assert.True(success);
                string export = cli.Export();
                Assert.True(!string.IsNullOrEmpty(export));
                cli.Logout();
            });
        }
    }
}