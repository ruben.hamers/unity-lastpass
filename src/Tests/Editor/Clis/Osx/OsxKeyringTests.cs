using System.Reflection;
using HamerSoft.LastPassUnity.Core;
using HamerSoft.LastPassUnity.Core.CLI.Osx;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Tests
{
    public class OsxKeyringTests
    {
        internal class MockOsxKeyring : OsxKeyring
        {
            internal bool AddPassword(Credentials credentials)
            {
                return (bool) typeof(OsxKeyring)
                    .GetMethod("AddPassword", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, new object[] {credentials});
            }

            internal Credentials GetPassword()
            {
                return (Credentials) typeof(OsxKeyring)
                    .GetMethod("GetPassword", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, null);
            }

            internal bool DeletePassword(Credentials credentials)
            {
                return (bool) typeof(OsxKeyring)
                    .GetMethod("DeletePassword", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, new object[] {credentials});
            }

            public MockOsxKeyring(string keyringSystem) : base(keyringSystem)
            {
            }
        }

        [Test,Order(0)]
        public void CheckForCorrectUserName()
        {
            Debug.Log(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            Assert.Pass();
        }

        [Test,Order(1)]
        public void Test_Install()
        {
            var cli = new OsxKeyring(TestingEnv.KeyringSystem);
            cli.Install(TestingEnv.LinuxOsPassword, Assert.True);
        }

        [Test,Order(2)]
        public void Test_IsInstalled()
        {
            var cli = new OsxKeyring(TestingEnv.KeyringSystem);
            if (cli.IsInstalled(true))
                Assert.Pass();
            else
                cli.Install(TestingEnv.LinuxOsPassword, success => { Assert.True(cli.IsInstalled(true)); });
        }

        [Test,Order(3)]
        public void Test_AddPassWord()
        {
            var cli = new MockOsxKeyring(TestingEnv.KeyringSystem);
            Assert.True(cli.AddPassword(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword)));
            cli.DeletePassword(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword));
        }

        [Test,Order(4)]
        public void Test_DeletePassWord()
        {
            var cli = new MockOsxKeyring(TestingEnv.KeyringSystem);
            cli.AddPassword(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword));
            Assert.True(cli.DeletePassword(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword)));
        }

        [Test,Order(5)]
        public void Test_GetPassWord()
        {
            var cli = new MockOsxKeyring(TestingEnv.KeyringSystem);
            cli.AddPassword(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword));
            var credentials = cli.GetPassword();
            if (credentials == null)
                Assert.Fail("credentials is null!");
            else
            {
                Assert.AreEqual(TestingEnv.LpUsername, credentials.Username);
                Assert.AreEqual(TestingEnv.LpPassword, credentials.Password);

                cli.DeletePassword(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword));
            }
        }
    }
}