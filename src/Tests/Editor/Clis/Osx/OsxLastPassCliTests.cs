using System;
using System.Reflection;
using HamerSoft.LastPassUnity.Core;
using HamerSoft.LastPassUnity.Core.CLI.Linux;
using HamerSoft.LastPassUnity.Core.CLI.Osx;
using HamerSoft.LastPassUnity.Core.CLI.WindowsUbuntu;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Tests
{
    public class OsxLastPassCliTests
    {
        internal class MockOsxLastPassCli : OsxLastPassCli
        {
            internal void Login(Credentials credentials, OtpUi otpUi, Action<bool, Exception> internalCallback)
            {
                try
                {
                    typeof(AbstractLastPassCli).GetMethod("Login", BindingFlags.NonPublic | BindingFlags.Instance)
                        .Invoke(this, new object[] {credentials, otpUi, internalCallback});
                }
                catch (Exception e)
                {
                    Debug.LogError($"ERROR! => {e.Message} | {e}");
                    throw;
                }
                
            }

            internal void Logout()
            {
                typeof(AbstractLastPassCli)
                    .GetMethod("Logout", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, null);
            }

            internal string Export()
            {
                return (string) typeof(AbstractLastPassCli)
                    .GetMethod("Export", BindingFlags.NonPublic | BindingFlags.Instance)
                    .Invoke(this, null);
            }
        }

        [Test,Order(0)]
        public void Test_Install()
        {
            var cli = new OsxLastPassCli();
            cli.Install(TestingEnv.MacOsPassword, Assert.True);
        }

        [Test,Order(1)]
        public void Test_IsInstalled()
        {
            var cli = new OsxLastPassCli();
            if (cli.IsInstalled(true))
                Assert.Pass();
            else
                cli.Install(TestingEnv.MacOsPassword, success => { Assert.True(cli.IsInstalled()); });
        }

        [Test,Order(2)]
        public void TestLogin()
        {
            var cli = new MockOsxLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null,
                (success, exception) => { Assert.True(success); });
            cli.Logout();
        }

        [Test,Order(3)]
        public void TestLogout()
        {
            var cli = new MockOsxLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null, (success, exception) =>
            {
                Assert.True(success);
                cli.Logout();
            });
        }

        [Test,Order(4)]
        public void Test_Export()
        {
            var cli = new MockOsxLastPassCli();
            cli.Login(new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword), null, (success, exception) =>
            {
                Assert.True(success);
                string export = cli.Export();
                Assert.True(!string.IsNullOrEmpty(export));
                cli.Logout();
            });
        }
    }
}