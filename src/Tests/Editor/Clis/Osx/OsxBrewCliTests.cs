﻿using HamerSoft.LastPassUnity.Core.CLI.Osx;
using HamerSoft.LastPassUnity.Core.CLI;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Tests
{
    public class OsxBrewCliTests
    {
        private OsxBrewCli _cli;

        [SetUp]
        public void Setup()
        {
            _cli = new OsxBrewCli(); 
        }
        
        [Test]
        public void Test_IsInstalled()
        {          
                if (_cli.IsInstalled(true))
                    Assert.Pass();
                else
                    _cli.Install(TestingEnv.LinuxOsPassword, success => { Assert.True(_cli.IsInstalled(true)); });
        }

        [Test]
        public void Test_Install()
        {
            _cli.Install(TestingEnv.LinuxOsPassword, Assert.True);
        }
    }
}