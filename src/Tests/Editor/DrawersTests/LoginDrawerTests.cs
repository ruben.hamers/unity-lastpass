using System.Reflection;
using HamerSoft.LastPassUnity.Core.Editor;
using NUnit.Framework;
using UnityEditor;

namespace HamerSoft.LastPassUnity.Tests
{
    public class LoginDrawerTests
    {
        internal class MockLoginDrawer : LoginDrawer
        {
            internal bool IsLoggingIn()
            {
                return (bool) typeof(ConfigDrawer).GetField("_isLoggingIn", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this);
            }

        }
        [Test]
        public void DrawerAuthenticates_WhenCredentialsArePresent()
        {
            if (Core.LastPass.HasCredentials)
            {
                var drawer = new MockLoginDrawer();
                bool startedLogin = false;
                EditorApplication.update = () =>
                {
                    if (!startedLogin)
                        return;
                    if (drawer.IsLoggingIn())
                    {
                        startedLogin = true;
                        return;
                    }

                    if (startedLogin && Core.LastPass.IsSupported)
                    {
                        Assert.True(Core.LastPass.IsLoggedIn);
                        EditorApplication.update = null;
                    }
                };
                drawer.Draw();
            }
            else
                Assert.Pass();
                
        }
    }
}