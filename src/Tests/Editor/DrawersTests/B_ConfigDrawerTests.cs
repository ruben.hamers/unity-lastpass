using System.IO;
using System.Reflection;
using HamerSoft.LastPassUnity.Core;
using HamerSoft.LastPassUnity.Core.Editor;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.LastPassUnity.Tests
{
    public class B_ConfigDrawerTests
    {
        private Config _config;
        private string _testDir;

        [SetUp]
        public void Setup()
        {
            _testDir = Path.Combine(Application.dataPath, TestingEnv.ResourcesTestsPath);
            _config = Config.Create(Path.Combine(TestingEnv.ResourcesTestsPath.Replace("Resources/", ""),
                TestingEnv.AssetName.Replace(".asset", "")));
        }

        [Test]
        public void WhenConfigIsCreated_ItIsSavedInTheCorrectDirectory()
        {
            Assert.NotNull(File.Exists(Path.Combine(_testDir, TestingEnv.AssetName)));
        }

        [Test]
        public void WhenConfigIsCreated_SomeFiltersAreAdded()
        {
            Assert.NotNull(_config.Filters);
        }

        [TearDown]
        public void TearDown()
        {
            if (Directory.Exists(_testDir))
                Directory.Delete(_testDir, true);
        }
    }
}