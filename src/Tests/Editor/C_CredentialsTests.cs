using HamerSoft.LastPassUnity.Core;
using NUnit.Framework;

namespace HamerSoft.LastPassUnity.Tests
{
    public class C_CredentialsTests
    {
        private Credentials _credentials;

        [SetUp]
        public void Setup()
        {
            _credentials = new Credentials("UncleBob", "CleanCoder");
        }

        [Test]
        public void WhenCredentialsAreCreated_TrustDeviceIsEnabled()
        {
            Assert.True(_credentials.IsTrustedDevice);
        }

        [Test]
        public void WhenCredentialsAreCreated_DescriptionIsSet()
        {
            Assert.NotNull(_credentials.Description);
        }

        [Test]
        public void WhenCredentialsAreCreated_IdIsSet()
        {
            Assert.NotNull(_credentials.Id);
        }

        [Test]
        public void WhenCredentialsAreJoined_TheyAreContainedInTheResult()
        {
            Assert.True(_credentials.Join().Contains(_credentials.Password) &&
                        _credentials.Join().Contains(_credentials.Password));
        }

        [Test]
        public void WhenCredentialsAreDisJoined_TheyAreCorrect()
        {
            var c = Credentials.Disjoin(_credentials.Join());
            Assert.AreEqual(_credentials.Username, c.Username);
            Assert.AreEqual(_credentials.Password, c.Password);
        }

        [Test]
        public void CredentialsId_OnlyAcceptsGUID()
        {
            string guid = System.Guid.NewGuid().ToString();
            string random = "HamerSoft";
            string oldId = _credentials.Id;
            _credentials.SetId(random);
            Assert.AreEqual(oldId,_credentials.Id);
            _credentials.SetId(guid);
            Assert.AreEqual(guid,_credentials.Id);
        }
    }
}