using System.Linq;
using HamerSoft.LastPassUnity.Core;
using NUnit.Framework;

namespace HamerSoft.LastPassUnity.Tests
{
    public class Z_LastPassTests
    {
        private Credentials _credentials, _invalidCredentials;
        private Config _config;
        private bool _originalCli;

        [SetUp]
        public void Setup()
        {
            _credentials = new Credentials(TestingEnv.LpUsername, TestingEnv.LpPassword);
            _invalidCredentials = new Credentials("HamerSoft", "LastPass");
            _config = Config.Create("Resources/LastPass");
            _config.Filters = _config.Filters.Concat(new[] {TestingEnv.LpUsername}).ToArray();
            _originalCli = _config.UseCli;
        }

        [Test]
        public void LoginSucceedsWith_Cli_Options()
        {
            _config.UseCli = true;
            LastPassUnity.Core.LastPass.Login(_credentials, null, (success, exception) =>
            {
                Assert.True(success);
                Assert.IsNull(exception);
            });
        }

        [Test]
        public void LoginSucceedsWithout_Cli_Options()
        {
            _config.UseCli = false;
            LastPassUnity.Core.LastPass.Login(_credentials, null, (success, exception) =>
            {
                Assert.True(success);
                Assert.IsNull(exception);
            });
        }

        [Test]
        public void AfterLogin_WithCLI_SuggestionsAreAvailable()
        {
            _config.UseCli = true;
            LastPassUnity.Core.LastPass.Login(_credentials, null, (success, exception) =>
            {
                Assert.True(success);
                Assert.IsNull(exception);
                Assert.NotNull(LastPassUnity.Core.LastPass.GetUserNamePasswordSuggestions());
            });
        }

        [Test]
        public void AfterLogin_WithoutCLI_SuggestionsAreAvailable()
        {
            _config.UseCli = false;
            LastPassUnity.Core.LastPass.Login(_credentials, null, (success, exception) =>
            {
                Assert.True(success);
                Assert.IsNull(exception);
                Assert.NotNull(LastPassUnity.Core.LastPass.GetUserNamePasswordSuggestions());
            });
        }

        [Test]
        public void UpdateAccounts_LogsIn_TheUserAgain()
        {
            _config.UseCli = false;
            LastPassUnity.Core.LastPass.Login(_credentials, null, (success, exception) =>
            {
                Assert.True(success);
                Assert.IsNull(exception);
                LastPassUnity.Core.LastPass.UpdateAccounts(null, (updated, exception1) =>
                {
                    Assert.True(updated);
                    Assert.IsNull(exception1);
                });
            });
        }

        [Test]
        public void WhenLoggedOut_TheUserCannot_Access_HisData()
        {
            _config.UseCli = false;
            LastPassUnity.Core.LastPass.Login(_credentials, null, (success, exception) =>
            {
                LastPassUnity.Core.LastPass.Logout();
                Assert.False(LastPassUnity.Core.LastPass.IsLoggedIn);
                Assert.AreEqual(0, LastPassUnity.Core.LastPass.GetUserNamePasswordSuggestions().Count);
            });
        }

        [TearDown]
        public void TearDown()
        {
            _config.UseCli = _originalCli;
        }
    }
}