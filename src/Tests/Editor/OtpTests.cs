using HamerSoft.LastPassUnity.Core;
using HamerSoft.LastPassUnity.Core.Editor;
using NUnit.Framework;

namespace HamerSoft.LastPassUnity.Tests
{
    public class OtpTests
    {
        private Credentials _credentials;

        [SetUp]
        public void Setup()
        {
            _credentials = new Credentials(TestingEnv.LpOtpUsername, TestingEnv.LpOtpPassword);
            _credentials.SetIsTrustedDevice(false);
        }

        [Test]
        public void OtpUI_EditorWindow_IsShown_When_Logging_Into_TheEditor()
        {
            if (TestingEnv.InCiPipeline)
                Assert.Pass();
            else
                LastPassUnity.Core.LastPass.Login(_credentials, new EditorOtpUi(), (success, exception) =>
                {
                    Assert.True(success);
                    Assert.Null(exception);
                    Core.LastPass.Logout();
                });
        }
    }
}