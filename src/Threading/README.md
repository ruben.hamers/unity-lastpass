﻿# Threading
This is a simple API that allows for easy multi-threading in Unity3D.
You can use the [Dispatcher](Core/Dispatcher.cs) to run an action async or return to the main thread.

When you run an action async you can supply a **continuation method** that will continue on the **main thread** after the action has finished.
Also, you can add an optional **error handling** method to resolve exceptions, this method is also run on the **main thread**.

Another option is to run c# native Task's or threads and simply use the Dispatcher to return to the main thread.

# Tests
See [Tests](Tests).

# ToDo
- Implement actions with return types.
