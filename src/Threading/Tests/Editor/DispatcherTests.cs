﻿using System;
using NUnit.Framework;
using UnityEngine;
using Object = UnityEngine.Object;

namespace HamerSoft.Threading.Tests
{
    public class DispatcherTests
    {
        /// <summary>
        /// gameobject generated when trying to manifest an exception for not being on the main thread
        /// </summary>
        private GameObject ExceptionGameObject;

        [SetUp]
        public void Setup()
        {
            if (ExceptionGameObject)
                Object.DestroyImmediate(ExceptionGameObject);
        }

        [Test]
        public void DispatcherMethod_IsExecutedOnAnotherThread()
        {
            int counter = 0;
            Dispatcher.RunAsync(
                () =>
                {
                    Assert.False(TryMainThreadAction());
                    counter++;
                },
                () => { Assert.AreEqual(0, counter); });
        }

        [Test]
        public void DispatcherContinuationMethod_IsRun_On_MainThread()
        {
            int counter = 0;
            Dispatcher.RunAsync(() => counter++, () =>
            {
                Assert.True(TryMainThreadAction());
                Assert.AreEqual(1, counter);
            });
        }

        [Test]
        public void DispatcherErrorHandler_Catches_Thrown_Exceptions()
        {
            int counter = 0;
            Dispatcher.RunAsync(() =>
            {
                counter++;
                throw new Exception("Testing Exception");
            }, () => { Assert.AreEqual(1, counter); }, exception =>
            {
                Assert.AreEqual("Testing Exception", exception.Message);
            });
        }

        [Test]
        public void DispatcherErrorHandler_Catches_Thrown_Exceptions_OnMainThread()
        {
            int counter = 0;
            Dispatcher.RunAsync(() =>
            {
                counter++;
                throw new Exception("Testing Exception");
            }, () => { Assert.AreEqual(1, counter); }, exception =>
            {
                Assert.AreEqual("Testing Exception", exception.Message);
                Assert.True(TryMainThreadAction());
            });
        }

        private bool TryMainThreadAction()
        {
            try
            {
                ExceptionGameObject = new GameObject("Threading Exception GameObject");
                Object.DestroyImmediate(ExceptionGameObject);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [TearDown]
        public void TearDown()
        {
            if (ExceptionGameObject)
                Object.DestroyImmediate(ExceptionGameObject);
        }
    }
}