﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace HamerSoft.Threading
{
    public sealed class MonoAdapter : MonoBehaviour
    {
        public event Action ApplicationQuited;
        private List<Action> _updates { get; } = new List<Action>();

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void OnApplicationQuit()
        {
            _updates.Clear();
            ApplicationQuited?.Invoke();
        }

        private void Update()
        {
            _updates.ForEach(u =>
            {
                try
                {
                    u();
                }
                catch (Exception e)
                {
                    Debug.LogError($"Exception in UpdateLoop {e} with message {e.Message}.");
                }
            });
        }

        public void AddUpdate(Action action)
        {
            if (action != null)
                _updates.Add(action);
        }
    }
}