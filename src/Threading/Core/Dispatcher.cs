using UnityEngine;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace HamerSoft.Threading
{
    public static class Dispatcher
    {
        private static Queue<Action> _queue;
        private static bool IsInitialized => _queue != null;
        private static MonoAdapter _adapter;

        private static void Initialize()
        {
            if (IsInitialized)
                return;
            if (Application.isPlaying)
            {
                if (_adapter == null)
                {
                    _adapter = new GameObject("DispatcherAdapter").AddComponent<MonoAdapter>();
                    _adapter.ApplicationQuited += OnApplicationQuit;
                    _adapter.AddUpdate(Update);
                }
            }
            else
            {
                AssignEditorModeUpdate();
            }

            _queue = new Queue<Action>();
        }

        private static void DeInitialize()
        {
            if (!IsInitialized)
                return;
            if (_adapter)
            {
                _adapter.ApplicationQuited -= OnApplicationQuit;
                UnityEngine.Object.DestroyImmediate(_adapter.gameObject);
                _adapter = null;
            }

            lock (_queue)
                _queue = null;
            UnAssignEditorModeUpdate();
        }

        /// <summary>
        /// Assign to Editor update loop to make sure functions can be threaded in the editor as well
        /// </summary>
        private static void AssignEditorModeUpdate()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                UnityEditor.EditorApplication.update += Update;
#endif
        }

        /// <summary>
        /// UnAssign from the editor update loop
        /// </summary>
        private static void UnAssignEditorModeUpdate()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                UnityEditor.EditorApplication.update -= Update;
#endif
        }

        /// <summary>
        /// Unity update function
        /// </summary>
        private static void Update()
        {
            lock (_queue)
                while (_queue.Count > 0)
                    _queue.Dequeue().Invoke();
        }

        /// <summary>
        /// Locks the queue and adds a function to the queue
        /// </summary>
        /// <param name="action">function that will be executed from the main thread.</param>
        private static void Enqueue(Action action)
        {
            lock (_queue)
                _queue.Enqueue(action);
        }

        /// <summary>
        /// Locks the queue and adds the Action to the queue
        /// </summary>
        /// <param name="action">function that will be executed from the main thread.</param>
        public static void ToMainThread(Action action)
        {
            Enqueue(action);
        }

        /// <summary>
        /// Teardown function to cleanup when application quits
        /// </summary>
        private static void OnApplicationQuit()
        {
            DeInitialize();
        }

        /// <summary>
        /// Run an action Async, with an optional error Handler that will run on the MAIN THREAD
        /// </summary>
        /// <param name="action">function to run async</param>
        /// <param name="errorHandler">error handler to run on the main thread (Meaning you can access Unity here)</param>
        /// <returns>Task that can be awaited if needed</returns>
        public static Task RunAsync(Action action, Action<Exception> errorHandler = null)
        {
            TryEditorInitialize();
            var taskCompletionSource = new TaskCompletionSource<bool>();

            void SafeAction()
            {
                try
                {
                    action();
                    taskCompletionSource.TrySetResult(true);
                }
                catch (Exception e)
                {
                    taskCompletionSource.TrySetException(e);
                    if (errorHandler != null)
                        ToMainThread(() => { errorHandler(e); });
                }
            }

            Enqueue(SafeAction);
            return taskCompletionSource.Task;
        }

        /// <summary>
        /// Run an action Async, with an optional error Handler that will run on the MAIN THREAD
        /// </summary>
        /// <param name="action">function to run async</param>
        /// <param name="continuation">continuation function that runs on the main thread</param>
        /// <param name="errorHandler">error handler to run on the main thread (Meaning you can access Unity here)</param>
        /// <returns>Task that can be awaited if needed</returns>
        public static Task RunAsync(Action action, Action continuation, Action<Exception> errorHandler = null)
        {
            TryEditorInitialize();
            var taskCompletionSource = new TaskCompletionSource<bool>();

            void SafeAction()
            {
                try
                {
                    action();
                    taskCompletionSource.TrySetResult(true);
                    ToMainThread(continuation);
                }
                catch (Exception e)
                {
                    taskCompletionSource.TrySetException(e);
                    if (errorHandler != null)
                        ToMainThread(() => { errorHandler(e); });
                }
            }

            Enqueue(SafeAction);
            return taskCompletionSource.Task;
        }

        private static void TryEditorInitialize()
        {
#if UNITY_EDITOR
            try
            {
                if (!IsInitialized)
                    Initialize();
            }
            catch (Exception)
            {
                // ignored, probably failed because initialize was called on another thread!
            }
#endif
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void PreSceneStart()
        {
            Initialize();
        }
    }

    public static class TaskExtensions
    {
        public static Task ContinueOnMainThread(this Task t, Action action)
        {
            return t.ContinueWith(task => { Dispatcher.ToMainThread(action); });
        }
    }

    public static class ActionExtensions
    {
        public static Task RunAsync(this Action a, Action<Exception> errorHandler = null)
        {
            return Dispatcher.RunAsync(a, errorHandler);
        }

        public static Task RunAsync(this Action a, Action continuation, Action<Exception> errorHandler = null)
        {
            return Dispatcher.RunAsync(a, continuation, errorHandler);
        }
    }
}